<?php

use Illuminate\Database\Seeder;

class m_modulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_modul')->delete();
        DB::table('m_modul')->insert([
			[
				'modul_id' => 1,
				'nama_modul' => 'CS-Nasabah-Individu',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 2,
				'nama_modul' => 'CS-Nasabah-Badan-Hukum',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 3,
				'nama_modul' => 'CS-Nasabah-Badan-Hukum',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 4,
				'nama_modul' => 'CS-Nasabah-Giro',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 5,
				'nama_modul' => 'CS-Nasabah-Tabungan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 6,
				'nama_modul' => 'CS-Nasabah-Deposito',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 7,
				'nama_modul' => 'CS-Nasabah-Sertifikat',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 8,
				'nama_modul' => 'CS-Nasabah-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 9,
				'nama_modul' => 'CS-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 10,
				'nama_modul' => 'CS-Perhitungan-Serdep',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 11,
				'nama_modul' => 'CS-Lap-Black-List-BI',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 12,
				'nama_modul' => 'CS-Lap-Giro-Rupiah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 13,
				'nama_modul' => 'CS-Lap-Giro-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 14,
				'nama_modul' => 'CS-Lap-Nasabah-Tabungan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 15,
				'nama_modul' => 'CS-Lap-Nasabah-Deposito-Rupiah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 16,
				'nama_modul' => 'CS-Lap-Nasabah-Deposito-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 17,
				'nama_modul' => 'CS-Lap-Nasabah-Sertifikat-Deposito',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 18,
				'nama_modul' => 'CS-Lap-Nasabah-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 19,
				'nama_modul' => 'CS-Lap-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 20,
				'nama_modul' => 'TL-Transaksi-Pembayaran-Tunai',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 21,
				'nama_modul' => 'TL-Transaksi-Penerimaan-Tunai',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 22,
				'nama_modul' => 'TL-Transaksi-Non-Tunai',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 23,
				'nama_modul' => 'TL-Jual-Beli-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 24,
				'nama_modul' => 'TL-Pos-Administratif',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 25,
				'nama_modul' => 'TL-Laporan-Mutasi-Harian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 26,
				'nama_modul' => 'TL-Laporan-Daftar-Pembayaran',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 27,
				'nama_modul' => 'TL-Laporan-Daftar-Penerimaan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 28,
				'nama_modul' => 'TL-Laporan-Posisi-Saldo-Tabungan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 29,
				'nama_modul' => 'TL-Laporan-Saldo-Harian-Tabungan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 30,
				'nama_modul' => 'TL-Laporan-Saldo-Mutasi-Tabungan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 31,
				'nama_modul' => 'TL-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 32,
				'nama_modul' => 'GR-Pencatatan-Fasilitas-PRK',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 33,
				'nama_modul' => 'GR-Transaksi-Rekening-Giro',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 34,
				'nama_modul' => 'GR-Jurnal-Bagian-Giro',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 35,
				'nama_modul' => 'GR-Posisi-Saldo-Giro-Rupiah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 104,
				'nama_modul' => 'GR-Posisi-Saldo-Giro-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 36,
				'nama_modul' => 'GR-Saldo-Harian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 37,
				'nama_modul' => 'GR-Saldo-Mutasi-Rekening-Giro',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 38,
				'nama_modul' => 'GR-Saldo-Mutasi-Rekening-Koran',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 39,
				'nama_modul' => 'GR-laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 40,
				'nama_modul' => 'TR-Validasi-Test-Key',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 41,
				'nama_modul' => 'TR-Input-Prefund',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 42,
				'nama_modul' => 'TR-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 43,
				'nama_modul' => 'TR-Monitoring-Rekening-Giro-BI',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 44,
				'nama_modul' => 'TR-Pos-Administratif',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 45,
				'nama_modul' => 'TR-laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 46,
				'nama_modul' => 'TR-laporan-Test-Key',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 47,
				'nama_modul' => 'TR-laporan-Mutasi-RAAPKN-Debet',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 48,
				'nama_modul' => 'TR-laporan-Mutasi-RAAPKN-Kredit',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 49,
				'nama_modul' => 'DP-Buka-Tutup-Rek-Deposito-Berjangka',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 50,
				'nama_modul' => 'DP-Buka-Tutup-Rek-Deposito-Sertifikat',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 51,
				'nama_modul' => 'DP-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 52,
				'nama_modul' => 'DP-Laporan-Posisi-Saldo-Nasabah-Rupiah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 53,
				'nama_modul' => 'DP-Laporan-Posisi-Saldo-Nasabah-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 54,
				'nama_modul' => 'DP-Laporan-Posisi-Saldo-Nasabah-Sertifikat',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 55,
				'nama_modul' => 'DP-Laporan-Saldo-Mutasi-Nasabah-Rupiah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 56,
				'nama_modul' => 'DP-Laporan-Saldo-Mutasi-Nasabah-Valas',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 57,
				'nama_modul' => 'DP-Laporan-Saldo-Mutasi-Nasabah-Sertifikat',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 58,
				'nama_modul' => 'DP-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 59,
				'nama_modul' => 'KL-Kliring-Debit',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 60,
				'nama_modul' => 'KL-Kliring-Kredit',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 61,
				'nama_modul' => 'KL-Kliring-Pengembalian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 62,
				'nama_modul' => 'KL-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 63,
				'nama_modul' => 'KL-Laporan-Hasil-Kliring-Penyerahan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 64,
				'nama_modul' => 'KL-Laporan-Hasil-Kliring-Penerimaan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 65,
				'nama_modul' => 'KL-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
				[
				'modul_id' => 66,
				'nama_modul' => 'KR-Realisasi-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 67,
				'nama_modul' => 'KR-Angsuran-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 68,
				'nama_modul' => 'KR-Pos-Administratif',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 69,
				'nama_modul' => 'KR-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 70,
				'nama_modul' => 'KR-Laporan-Kartu-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 71,
				'nama_modul' => 'KR-Laporan-Tagihan-Kartu-Kredit',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 72,
				'nama_modul' => 'KR-Laporan-Posisi-Saldo-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 73,
				'nama_modul' => 'KR-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 74,
				'nama_modul' => 'AK-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 75,
				'nama_modul' => 'AK-Monitoring-Rekening-Perantara',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 76,
				'nama_modul' => 'AK-Laporan-Daftar-Perkiraan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 77,
				'nama_modul' => 'AK-Laporan-Rekening-Perantara',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 78,
				'nama_modul' => 'AK-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 79,
				'nama_modul' => 'AK-Laporan-Jurnal-Harian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 80,
				'nama_modul' => 'AK-Laporan-Laba-Rugi',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 81,
				'nama_modul' => 'AK-Laporan-Posisi-Keuangan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 82,
				'nama_modul' => 'AO-Kebutuhan-Modal-Kerja',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 83,
				'nama_modul' => 'AO-Tabel-Angsuran-Nasabah',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 84,
				'nama_modul' => 'AO-Informasi-Skedul-Angsuran',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 85,
				'nama_modul' => 'AO-Laporan-Saldo-Pinjaman',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 86,
				'nama_modul' => 'EX-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 87,
				'nama_modul' => 'EX-Perhitungan-Wesel-Export',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 88,
				'nama_modul' => 'EX-Pos-Administratif',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 89,
				'nama_modul' => 'EX-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 90,
				'nama_modul' => 'IM-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 91,
				'nama_modul' => 'IM-Telex-Import',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 92,
				'nama_modul' => 'IM-Pembebanan-Import',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 93,
				'nama_modul' => 'IM-Pos-Administratif',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 94,
				'nama_modul' => 'IM-Laporan-Jurnal-Bagian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 95,
				'nama_modul' => 'EDP-Bentuk-Data-Awal',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 96,
				'nama_modul' => 'EDP-Proses-Saldo-Awal',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 97,
				'nama_modul' => 'EDP-Edit-Perkiraan',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 98,
				'nama_modul' => 'EDP-Input-Black-List-BI',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 99,
				'nama_modul' => 'EDP-Peserta-Kliring',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 100,
				'nama_modul' => 'EDP-Input-Nilai-Tukar',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 101,
				'nama_modul' => 'EDP-Security-Level',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 102,
				'nama_modul' => 'EDP-Batas-Wewenang',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
			[
				'modul_id' => 103,
				'nama_modul' => 'EDP-Tutup-Harian',
				'dt_record' => date("Y-m-d H:i:s"),
				'user_record' => "Administrator",
				'dt_modified' => date("Y-m-d H:i:s"),
				'user_modified' => "Administrator",
			],
		]);
    }
}
