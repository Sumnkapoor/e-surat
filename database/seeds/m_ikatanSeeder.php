<?php

use Illuminate\Database\Seeder;

class m_ikatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('m_ikatan')->delete();
		  DB::table('m_ikatan')->insert([
		    [ 'id' => 1, 'kode' => "1", 'nama' => "Deposito Liquid", 'persen' => "100", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "2", 'nama' => "APHT", 'persen' => "80", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "3", 'nama' => "SKMHT", 'persen' => "60", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "4", 'nama' => "Notariil", 'persen' => "30", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "5", 'nama' => "Fiducia", 'persen' => "50", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "6", 'nama' => "Dibawah Tangan", 'persen' => "0", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],                              
		]);
    }
}
