<?php

use Illuminate\Database\Seeder;

class t_neracaAkhirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_neraca_akhir')->delete();
		DB::table('t_neraca_akhir')->insert([
            [
                'neraca_akhir_id' => 1, 
                'tanggal' => date("Y-m-d"), 
                'bulan' => date("n"), 
                'tahun' => date("Y"), 
                'tanggal_yt' => date("Y-m-d"), 
                'id_kelas' => 1, 
                'created_at' => date("Y-m-d H:i:s"), 
                'updated_at' => date("Y-m-d H:i:s"),
            ]

        ]);
    }
}
