<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);		
		$this->call(m_pendidikanSeeder::class);
		$this->call(m_jenisidentitasSeeder::class);
		$this->call(m_jenisrekeningSeeder::class);
		$this->call(m_agamaSeeder::class);
		$this->call(m_daftarKliringSeeder::class);
		$this->call(m_roleSeeder::class);
		$this->call(m_modulSeeder::class);
		$this->call(m_lembagaSeeder::class);
		$this->call(m_kelasSeeder::class);
		$this->call(m_userSeeder::class);
		$this->call(m_sumberDanaSeeder::class);
		$this->call(m_penghasilanSeeder::class);
		$this->call(m_pekerjaanSeeder::class);
		$this->call(m_jenisBadanUsahaSeeder::class);
		$this->call(m_statusSeeder::class);
		$this->call(m_sandiPemilikSeeder::class);
		$this->call(m_jenisPinjamanSeeder::class);
		$this->call(m_editPerkiraanSeeder::class);		
		$this->call(m_nasabahSeeder::class);	
		$this->call(m_kodeTransaksiSeeder::class);	
		$this->call(m_sandiTanggalSeeder::class);	
		$this->call(m_nilaiTukarSeeder::class);	
		$this->call(m_sandiBulanSeeder::class);	
		$this->call(m_sandiKantorCabangSeeder::class);	
		$this->call(m_sandiNominalSeeder::class);	
		$this->call(m_golonganDebiturSeeder::class);	
		$this->call(m_sifatKreditSeeder::class);
		$this->call(m_jenisPenggunaanSeeder::class);
		$this->call(m_periodeBayarSeeder::class);
		$this->call(m_lokasiDebiturSeeder::class);
		$this->call(m_penjaminSeeder::class);
		$this->call(m_jenisAngunanSeeder::class);
		$this->call(m_sektorEkonomiSeeder::class);
		$this->call(m_sandiTransaksiSeeder::class);
		$this->call(m_ikatanSeeder::class);
		$this->call(t_neracaAkhirSeeder::class);		
		$this->call(t_neracaAkhirDetailSeeder::class);
		$this->call(roleModulTableSeeder::class);
    }
}
