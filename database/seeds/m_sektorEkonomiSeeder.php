<?php

use Illuminate\Database\Seeder;

class m_sektorEkonomiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sektor_ekonomi')->delete();
		  DB::table('m_sektor_ekonomi')->insert([
		    [ 'id' => 1, 'kode' => "1001", 'nama' => "Pertanian, Perburuan & Kehutanan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "1002", 'nama' => "Perikanan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "1003", 'nama' => "Pertambangan & Penggalian", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "1004", 'nama' => "Industri Pengelolahan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "1005", 'nama' => "Listrik, Gas & Air", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "1006", 'nama' => "Kontruksi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode' => "1007", 'nama' => "Perdagangan Besar & Eceran", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 8, 'kode' => "1008", 'nama' => "Penyedia Akomodasi & Mamin", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 9, 'kode' => "1009", 'nama' => "Transportasi, Pergudangan & Komunikasi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 10, 'kode' => "1010", 'nama' => "Perantara Keuangan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 11, 'kode' => "1011", 'nama' => "Real Estate", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 12, 'kode' => "1012", 'nama' => "Admin Pemerintah, Pertanahan & Jamsos Wajib", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 13, 'kode' => "1013", 'nama' => "Jasa Pendidikan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 14, 'kode' => "1014", 'nama' => "Jasa Kesehatan & Kegiatan Sosial", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 15, 'kode' => "1015", 'nama' => "Jasa Kemasyarakatan, Sosbud & Hiburan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 16, 'kode' => "1016", 'nama' => "Jasa Perorangan Melayani Rumah Tangga", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],                
        [ 'id' => 18, 'kode' => "1018", 'nama' => "Kegiatan Usaha Yg Belum Jelas Batasanya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 19, 'kode' => "1019", 'nama' => "Bukan Lapangan Usaha - Rumah Tangga", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 20, 'kode' => "1020", 'nama' => "Bukan Lapangan Usaha - Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}
