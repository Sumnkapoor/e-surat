<?php

use Illuminate\Database\Seeder;

class m_kodeTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_kode_transaksi')->delete();
		  DB::table('m_kode_transaksi')->insert([
		    [ 'id' => 1, 'kode_transaksi' => "1", 'nama_transaksi' => 'Pembayaran Rekening', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 2, 'kode_transaksi' => "2", 'nama_transaksi' => 'Pembayaran Bunga Deposito', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 3, 'kode_transaksi' => "3", 'nama_transaksi' => 'Setoran Ke Kas Rupiah', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 4, 'kode_transaksi' => "4", 'nama_transaksi' => 'Pembayaran Incoming Transfer', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 5, 'kode_transaksi' => "5", 'nama_transaksi' => 'Pembayaran Deposito', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 6, 'kode_transaksi' => "6", 'nama_transaksi' => 'Pembayaran Lain-Lain', 'jenis_transaksi' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 7, 'kode_transaksi' => "11", 'nama_transaksi' => 'Penerimaan Rekening', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 8, 'kode_transaksi' => "12", 'nama_transaksi' => 'Penerimaan Untuk Transfer', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 9, 'kode_transaksi' => "13", 'nama_transaksi' => 'Pengambilan dari Kas Rupiah', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 10, 'kode_transaksi' => "14", 'nama_transaksi' => 'Penerimaan Angsuran', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 11, 'kode_transaksi' => "15", 'nama_transaksi' => 'Penerimaan Deposito', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 12, 'kode_transaksi' => "16", 'nama_transaksi' => 'Penerimaan Lain-Lain', 'jenis_transaksi' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
		]);
    }
}
