<?php

use Illuminate\Database\Seeder;

class m_roleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		//DB::table('m_role')->delete();
        DB::table('m_role')->insert([
		[
            'role_id' => 1,
            'nama_role' => 'EDP',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 2,
            'nama_role' => 'Customer Service',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 3,
            'nama_role' => 'Teller',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 4,
            'nama_role' => 'Giro',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 5,
            'nama_role' => 'Transfer',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 6,
            'nama_role' => 'Deposito',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 7,
            'nama_role' => 'Kliring',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 8,
            'nama_role' => 'Admin Kredit',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 9,
            'nama_role' => 'Akuntansi',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 10,
            'nama_role' => 'Account Officer',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 11,
            'nama_role' => 'Export',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'role_id' => 12,
            'nama_role' => 'Import',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ]
		
		]);
    }
}
