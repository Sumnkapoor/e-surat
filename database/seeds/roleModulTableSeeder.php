<?php

use Illuminate\Database\Seeder;

class roleModulTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('m_roles_moduls')->insert([
		[
			'role_id' => 1,
			'modul_id' => 95,
		],[
			'role_id' => 1,
			'modul_id' => 96,
		],[
			'role_id' => 1,
			'modul_id' => 97,
		],[
			'role_id' => 1,
			'modul_id' => 98,
		],[
			'role_id' => 1,
			'modul_id' => 99,
		],[
			'role_id' => 1,
			'modul_id' => 100,
		],[
			'role_id' => 1,
			'modul_id' => 101,
		],[
			'role_id' => 1,
			'modul_id' => 102,
		],[
			'role_id' => 1,
			'modul_id' => 103,
		],[
			'role_id' => 2,
			'modul_id' => 1,
		],[
			'role_id' => 2,
			'modul_id' => 2,
		],[
			'role_id' => 2,
			'modul_id' => 3,
		],[
			'role_id' => 2,
			'modul_id' => 4,
		],[
			'role_id' => 2,
			'modul_id' => 5,
		],[
			'role_id' => 2,
			'modul_id' => 6,
		],[
			'role_id' => 2,
			'modul_id' => 7,
		],[
			'role_id' => 2,
			'modul_id' => 8,
		],[
			'role_id' => 2,
			'modul_id' => 9,
		],[
			'role_id' => 2,
			'modul_id' => 10,
		],[
			'role_id' => 2,
			'modul_id' => 11,
		],[
			'role_id' => 2,
			'modul_id' => 12,
		],[
			'role_id' => 2,
			'modul_id' => 13,
		],[
			'role_id' => 2,
			'modul_id' => 14,
		],[
			'role_id' => 2,
			'modul_id' => 15,
		],[
			'role_id' => 2,
			'modul_id' => 16,
		],[
			'role_id' => 2,
			'modul_id' => 17,
		],[
			'role_id' => 2,
			'modul_id' => 18,
		],[
			'role_id' => 2,
			'modul_id' => 19,
		],[
			'role_id' => 3,
			'modul_id' => 20,
		],[
			'role_id' => 3,
			'modul_id' => 21,
		],[
			'role_id' => 3,
			'modul_id' => 22,
		],[
			'role_id' => 3,
			'modul_id' => 23,
		],[
			'role_id' => 3,
			'modul_id' => 24,
		],[
			'role_id' => 3,
			'modul_id' => 25,
		],[
			'role_id' => 3,
			'modul_id' => 26,
		],[
			'role_id' => 3,
			'modul_id' => 27,
		],[
			'role_id' => 3,
			'modul_id' => 28,
		],[
			'role_id' => 3,
			'modul_id' => 29,
		],[
			'role_id' => 3,
			'modul_id' => 30,
		],[
			'role_id' => 3,
			'modul_id' => 31,
		],[
			'role_id' => 4,
			'modul_id' => 32,
		],[
			'role_id' => 4,
			'modul_id' => 33,
		],[
			'role_id' => 4,
			'modul_id' => 34,
		],[
			'role_id' => 4,
			'modul_id' => 35,
		],[
			'role_id' => 4,
			'modul_id' => 104,
		],[
			'role_id' => 4,
			'modul_id' => 36,
		],[
			'role_id' => 4,
			'modul_id' => 37,
		],[
			'role_id' => 4,
			'modul_id' => 38,
		],[
			'role_id' => 4,
			'modul_id' => 39,
		],[
			'role_id' => 5,
			'modul_id' => 40,
		],[
			'role_id' => 5,
			'modul_id' => 41,
		],[
			'role_id' => 5,
			'modul_id' => 42,
		],[
			'role_id' => 5,
			'modul_id' => 43,
		],[
			'role_id' => 5,
			'modul_id' => 44,
		],[
			'role_id' => 5,
			'modul_id' => 45,
		],[
			'role_id' => 5,
			'modul_id' => 46,
		],[
			'role_id' => 5,
			'modul_id' => 47,
		],[
			'role_id' => 5,
			'modul_id' => 48,
		],[
			'role_id' => 6,
			'modul_id' => 49,
		],[
			'role_id' => 6,
			'modul_id' => 50,
		],[
			'role_id' => 6,
			'modul_id' => 51,
		],[
			'role_id' => 6,
			'modul_id' => 52,
		],[
			'role_id' => 6,
			'modul_id' => 53,
		],[
			'role_id' => 6,
			'modul_id' => 54,
		],[
			'role_id' => 6,
			'modul_id' => 55,
		],[
			'role_id' => 6,
			'modul_id' => 56,
		],[
			'role_id' => 6,
			'modul_id' => 57,
		],[
			'role_id' => 6,
			'modul_id' => 58,
		],[
			'role_id' => 7,
			'modul_id' => 59,
		],[
			'role_id' => 7,
			'modul_id' => 60,
		],[
			'role_id' => 7,
			'modul_id' => 61,
		],[
			'role_id' => 7,
			'modul_id' => 62,
		],[
			'role_id' => 7,
			'modul_id' => 63,
		],[
			'role_id' => 7,
			'modul_id' => 64,
		],[
			'role_id' => 7,
			'modul_id' => 65,
		],[
			'role_id' => 8,
			'modul_id' => 66,
		],[
			'role_id' => 8,
			'modul_id' => 67,
		],[
			'role_id' => 8,
			'modul_id' => 68,
		],[
			'role_id' => 8,
			'modul_id' => 69,
		],[
			'role_id' => 8,
			'modul_id' => 70,
		],[
			'role_id' => 8,
			'modul_id' => 71,
		],[
			'role_id' => 8,
			'modul_id' => 72,
		],[
			'role_id' => 8,
			'modul_id' => 73,
		],[
			'role_id' => 9,
			'modul_id' => 74,
		],[
			'role_id' => 9,
			'modul_id' => 75,
		],[
			'role_id' => 9,
			'modul_id' => 76,
		],[
			'role_id' => 9,
			'modul_id' => 77,
		],[
			'role_id' => 9,
			'modul_id' => 78,
		],[
			'role_id' => 9,
			'modul_id' => 79,
		],[
			'role_id' => 9,
			'modul_id' => 80,
		],[
			'role_id' => 9,
			'modul_id' => 81,
		],[
			'role_id' => 10,
			'modul_id' => 82,
		],[
			'role_id' => 10,
			'modul_id' => 83,
		],[
			'role_id' => 10,
			'modul_id' => 84,
		],[
			'role_id' => 10,
			'modul_id' => 85,
		],[
			'role_id' => 11,
			'modul_id' => 86,
		],[
			'role_id' => 11,
			'modul_id' => 87,
		],[
			'role_id' => 11,
			'modul_id' => 88,
		],[
			'role_id' => 11,
			'modul_id' => 89,
		],[
			'role_id' => 12,
			'modul_id' => 90,
		],[
			'role_id' => 12,
			'modul_id' => 91,
		],[
			'role_id' => 12,
			'modul_id' => 92,
		],[
			'role_id' => 12,
			'modul_id' => 93,
		],[
			'role_id' => 12,
			'modul_id' => 94,
		]
		]);		
		
    }
}
