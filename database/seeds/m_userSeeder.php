<?php

use Illuminate\Database\Seeder;

class m_userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
		DB::table('users')->insert([
            ['name' => 'EDP', 'email' => Str::random(10).'@gmail.com', 'username' => 'edp', 'kelas_id' => 1, 'id_role' => 1, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
	    ['name' => 'EDP', 'email' => Str::random(10).'@gmail.com', 'username' => 'edp', 'kelas_id' => 2, 'id_role' => 1, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
	    ['name' => 'Customer Service', 'email' => Str::random(10).'@gmail.com', 'username' => 'cs', 'kelas_id' => 1, 'id_role' => 2, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Teller', 'email' => Str::random(10).'@gmail.com', 'username' => 'tl', 'kelas_id' => 1, 'id_role' => 3, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Giro', 'email' => Str::random(10).'@gmail.com', 'username' => 'gr', 'kelas_id' => 1, 'id_role' => 4, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Transfer', 'email' => Str::random(10).'@gmail.com', 'username' => 'tr', 'kelas_id' => 1, 'id_role' => 5, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Deposito', 'email' => Str::random(10).'@gmail.com', 'username' => 'dp', 'kelas_id' => 1, 'id_role' => 6, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Kliring', 'email' => Str::random(10).'@gmail.com', 'username' => 'kl', 'kelas_id' => 1, 'id_role' => 7, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Admin Kredit', 'email' => Str::random(10).'@gmail.com', 'username' => 'kr', 'kelas_id' => 1, 'id_role' => 8, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Akuntansi', 'email' => Str::random(10).'@gmail.com', 'username' => 'ak', 'kelas_id' => 1, 'id_role' => 9, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Account Officer', 'email' => Str::random(10).'@gmail.com', 'username' => 'ao', 'kelas_id' => 1, 'id_role' => 10, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Export', 'email' => Str::random(10).'@gmail.com', 'username' => 'ex', 'kelas_id' => 1, 'id_role' => 11, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Import', 'email' => Str::random(10).'@gmail.com', 'username' => 'im', 'kelas_id' => 1, 'id_role' => 12, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Customer Service', 'email' => Str::random(10).'@gmail.com', 'username' => 'cs', 'kelas_id' => 2, 'id_role' => 2, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Teller', 'email' => Str::random(10).'@gmail.com', 'username' => 'tl', 'kelas_id' => 2, 'id_role' => 3, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Giro', 'email' => Str::random(10).'@gmail.com', 'username' => 'gr', 'kelas_id' => 2, 'id_role' => 4, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Transfer', 'email' => Str::random(10).'@gmail.com', 'username' => 'tr', 'kelas_id' => 2, 'id_role' => 5, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Deposito', 'email' => Str::random(10).'@gmail.com', 'username' => 'dp', 'kelas_id' => 2, 'id_role' => 6, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Kliring', 'email' => Str::random(10).'@gmail.com', 'username' => 'kl', 'kelas_id' => 2, 'id_role' => 7, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Admin Kredit', 'email' => Str::random(10).'@gmail.com', 'username' => 'kr', 'kelas_id' => 2, 'id_role' => 8, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Akuntansi', 'email' => Str::random(10).'@gmail.com', 'username' => 'ak', 'kelas_id' => 2, 'id_role' => 9, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Account Officer', 'email' => Str::random(10).'@gmail.com', 'username' => 'ao', 'kelas_id' => 2, 'id_role' => 10, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Export', 'email' => Str::random(10).'@gmail.com', 'username' => 'ex', 'kelas_id' => 2, 'id_role' => 11, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
			['name' => 'Import', 'email' => Str::random(10).'@gmail.com', 'username' => 'im', 'kelas_id' => 2, 'id_role' => 12, 'email_verified_at' => date("Y-m-d H:i:s"), 'password' => '$2y$10$GSrRkC1AszM5ozpkRruATeDvkOJhSG2kvYsnSDoxP93/b2cb2kF2W',],
        ]);
    }
}
