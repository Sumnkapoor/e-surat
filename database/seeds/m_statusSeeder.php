<?php

use Illuminate\Database\Seeder;

class m_statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_status')->delete();
		  DB::table('m_status')->insert([
		 [
            'id' => 1,
            'name' => 'Belum Menikah',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
			
        ],
		[
            'id' => 2,
            'name' => 'Menikah',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'name' => 'Cerai',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'name' => 'Duda',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'name' => 'Janda',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 6,
            'name' => 'Lainnya',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ]
		]);
    }
}
