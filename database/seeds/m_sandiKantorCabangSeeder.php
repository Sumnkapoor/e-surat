<?php

use Illuminate\Database\Seeder;

class m_sandiKantorCabangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sandi_kantor_cabang')->delete();
		  DB::table('m_sandi_kantor_cabang')->insert([
		    [ 'id' => 1, 'kode_kantor' => "010 - Surabaya", 'sandi_pengirim' => 6, 'sandi_penerima' => 3, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode_kantor' => "011 - Semarang", 'sandi_pengirim' => 12, 'sandi_penerima' => 9, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode_kantor' => "012 - Malang", 'sandi_pengirim' => 18, 'sandi_penerima' => 15, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode_kantor' => "014 - Jakarta", 'sandi_pengirim' => 24, 'sandi_penerima' => 21, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}
