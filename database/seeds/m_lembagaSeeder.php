<?php

use Illuminate\Database\Seeder;

class m_lembagaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_lembaga')->delete();
        DB::table('m_lembaga')->insert([
		[
            'id' => 1,
            'nama_lembaga' => 'Universitas Hayam Wuruk Perbanas',
            'alamat_lembaga' => 'Wonorejo, Rungkut, Surabaya City, East Java 60296',
            'telp_lembaga' => '(031) 5947151',
            'domain' => 'bankstiep.perbanas.ac.id',
            'logo_header' => 'img/UHW_4.png',
            'logo_login' => 'img/UHW_5.png',
            'nama_bank' => 'BANK PERBANAS SURABAYA',			
            'pass_admin' => 'stiep',			
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),			
        ]        
		]);
    }
}
