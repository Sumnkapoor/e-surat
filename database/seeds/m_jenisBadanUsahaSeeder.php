<?php

use Illuminate\Database\Seeder;

class m_jenisBadanUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_jenis_badan_usaha')->delete();
		  DB::table('m_jenis_badan_usaha')->insert([
		 [
            'id' => 1,
            'nama' => 'PT',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
			
        ],
		[
            'id' => 2,
            'nama' => 'CV',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'nama' => 'Firma',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'nama' => 'Koperasi',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'nama' => 'Yayasan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 6,
            'nama' => 'Lainnya',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ]
		]);
    }
}
