<?php

use Illuminate\Database\Seeder;

class m_jenisPinjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_jenis_pinjaman')->delete();
		  DB::table('m_jenis_pinjaman')->insert([
		 [
            'pinjaman_id' => 1,
            'pinjaman_nama' => 'Pinjaman Installment',
            'user_record' => 'System',
            'dt_record' => date("Y-m-d H:i:s"),
            'user_modified' => 'System',
			'dt_modified' => date("Y-m-d H:i:s"),
			
        ],
		[
            'pinjaman_id' => 2,
            'pinjaman_nama' => 'Pinjaman Reguler',
            'user_record' => 'System',
            'dt_record' => date("Y-m-d H:i:s"),
            'user_modified' => 'System',
			'dt_modified' => date("Y-m-d H:i:s"),
        ],
		[
            'pinjaman_id' => 3,
            'pinjaman_nama' => 'Kartu Kredit',
            'user_record' => 'System',
            'dt_record' => date("Y-m-d H:i:s"),
            'user_modified' => 'System',
			'dt_modified' => date("Y-m-d H:i:s"),
        ]
		]);
    }
}
