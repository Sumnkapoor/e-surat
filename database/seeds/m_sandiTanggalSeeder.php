<?php

use Illuminate\Database\Seeder;

class m_sandiTanggalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sandi_tanggal')->delete();
		  DB::table('m_sandi_tanggal')->insert([
		    [ 'id' => 1, 'tanggal' => 1, 'sandi' => 10, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'tanggal' => 2, 'sandi' => 20, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'tanggal' => 3, 'sandi' => 30, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'tanggal' => 4, 'sandi' => 40, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'tanggal' => 5, 'sandi' => 50, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 6, 'tanggal' => 6, 'sandi' => 60, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 7, 'tanggal' => 7, 'sandi' => 70, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 8, 'tanggal' => 8, 'sandi' => 80, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 9, 'tanggal' => 9, 'sandi' => 90, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 10, 'tanggal' => 10, 'sandi' => 100, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 11, 'tanggal' => 11, 'sandi' => 110, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 12, 'tanggal' => 12, 'sandi' => 120, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 13, 'tanggal' => 13, 'sandi' => 130, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 14, 'tanggal' => 14, 'sandi' => 140, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 15, 'tanggal' => 15, 'sandi' => 150, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 16, 'tanggal' => 16, 'sandi' => 160, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 17, 'tanggal' => 17, 'sandi' => 170, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 18, 'tanggal' => 18, 'sandi' => 180, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 19, 'tanggal' => 19, 'sandi' => 190, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 20, 'tanggal' => 20, 'sandi' => 200, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 21, 'tanggal' => 21, 'sandi' => 210, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 22, 'tanggal' => 22, 'sandi' => 220, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 23, 'tanggal' => 23, 'sandi' => 230, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 24, 'tanggal' => 24, 'sandi' => 240, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 25, 'tanggal' => 25, 'sandi' => 250, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 26, 'tanggal' => 26, 'sandi' => 260, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 27, 'tanggal' => 27, 'sandi' => 270, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 28, 'tanggal' => 28, 'sandi' => 280, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 29, 'tanggal' => 29, 'sandi' => 290, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 30, 'tanggal' => 30, 'sandi' => 300, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
        [ 'id' => 31, 'tanggal' => 31, 'sandi' => 310, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				
		]);
    }
}
