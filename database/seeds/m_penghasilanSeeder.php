<?php

use Illuminate\Database\Seeder;

class m_penghasilanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_penghasilan')->delete();
		  DB::table('m_penghasilan')->insert([
		 [
            'id' => 1,
            'nama' => 'Penghasilan <= 5 Juta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
			
        ],
		[
            'id' => 2,
            'nama' => '5 Juta < Penghasilan <= 10 Juta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'nama' => '10 Juta < Penghasilan <= 25 Juta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'nama' => 'Penghasilan >25 Juta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'nama' => 'Tidak Ada Informasi',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ]		
		]);
    }
}
