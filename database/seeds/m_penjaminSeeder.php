<?php

use Illuminate\Database\Seeder;

class m_penjaminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('m_penjamin')->delete();
		  DB::table('m_penjamin')->insert([
		    [ 'id' => 1, 'kode' => "700", 'nama' => "Bank Umum", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "805", 'nama' => "Pemerintah Daerah", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "832", 'nama' => "Pemerintah - BUMN", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "834", 'nama' => "Pemerintah - BUMD", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "835", 'nama' => "Pemerintah Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "840", 'nama' => "Pemerintah Campuran", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode' => "860", 'nama' => "Perusahaan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 8, 'kode' => "870", 'nama' => "Koperasi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 9, 'kode' => "872", 'nama' => "Kelompok", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 10, 'kode' => "874", 'nama' => "Perorangan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 11, 'kode' => "880", 'nama' => "Asuransi Jiwa", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 12, 'kode' => "890", 'nama' => "Asuransi Kredit", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 13, 'kode' => "901", 'nama' => "Unit Syariah Bank Umum", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}
