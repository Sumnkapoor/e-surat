<?php

use Illuminate\Database\Seeder;

class m_jenisAngunanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('m_jenis_angunan')->delete();
		  DB::table('m_jenis_angunan')->insert([
		    [ 'id' => 1, 'kode' => "1", 'nama' => "SBI", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "2", 'nama' => "Tab/Dep Pada Bank Ybs", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "3", 'nama' => "Tab/Dep Pada Bank Lain", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "4", 'nama' => "Emas dan Logam Mulia", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "5", 'nama' => "Kendaraan Bermotor", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "6", 'nama' => "Tanah Dan Bangunan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode' => "7", 'nama' => "Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 8, 'kode' => "8", 'nama' => "Tanpa Angunan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],                
		]);
    }
}
