<?php

use Illuminate\Database\Seeder;

class m_golonganDebiturSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_golongan_debitur')->delete();
		  DB::table('m_golongan_debitur')->insert([
		    [ 'id' => 1, 'kode' => "800", 'nama' => "Pemerintah Pusat", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "805", 'nama' => "Pemerintah Daerah", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "832", 'nama' => "Perusahaan - BUMN", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "834", 'nama' => "Perusahaan - BUMD", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "835", 'nama' => "Pemerintah Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 6, 'kode' => "840", 'nama' => "Pemerintah Campuran", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 7, 'kode' => "860", 'nama' => "Perusahaan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 8, 'kode' => "870", 'nama' => "Koperasi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 9, 'kode' => "872", 'nama' => "Kelompok", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 10, 'kode' => "874", 'nama' => "Perorangan Peg/Pensiunan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 11, 'kode' => "875", 'nama' => "Perorangan Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}
