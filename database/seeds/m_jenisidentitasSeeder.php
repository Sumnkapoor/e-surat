<?php

use Illuminate\Database\Seeder;

class m_jenisidentitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('m_jenis_identitas')->delete();
		  DB::table('m_jenis_identitas')->insert([
		 [
            'id' => 1,
            'name' => 'KTP',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 2,
            'name' => 'SIM',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'name' => 'Passport',
           'created_at' => date("Y-m-d H:i:s"),
        ]
		
		]);
    }
}
