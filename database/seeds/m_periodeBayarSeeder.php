<?php

use Illuminate\Database\Seeder;

class m_periodeBayarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('m_periode_bayar')->delete();
		  DB::table('m_periode_bayar')->insert([
		    [ 'id' => 1, 'kode' => "1", 'nama' => "Harian", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "2", 'nama' => "Mingguan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "3", 'nama' => "Bulanan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "4", 'nama' => "Tri Wulanan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "5", 'nama' => "Semesteran", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "6", 'nama' => "Tahunan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode' => "7", 'nama' => "Sekaligus", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}
