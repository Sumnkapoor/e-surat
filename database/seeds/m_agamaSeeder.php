<?php

use Illuminate\Database\Seeder;

class m_agamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_agama')->delete();
		  DB::table('m_agama')->insert([
		 [
            'id' => 1,
            'name' => 'Islam',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
			
        ],
		[
            'id' => 2,
            'name' => 'Kristen Protestan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'name' => 'Kristen Katholik',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'name' => 'Hindu',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'name' => 'Budha',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 6,
            'name' => 'Konghucu',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ]
		
		]);
    }
}
