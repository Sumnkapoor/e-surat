<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::dropIfExists('m_role');
        Schema::create('m_role', function (Blueprint $table) {
            $table->bigIncrements('role_id');
			$table->string('nama_role');
            $table->timestamp('dt_record')->nullable();
			$table->string('user_record');
			$table->timestamp('dt_modified')->nullable();
			$table->string('user_modified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_role');
    }
}
