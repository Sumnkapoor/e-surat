<?php

    if(!function_exists('is_menu_request')){

        function is_menu_request($menu)

        {

            if(is_array($menu)){

                  $first_menu = request()->segment(1);

                  if( array_search($first_menu, $menu)){

                        if( request()->is($first_menu) || request()->is($first_menu.'/*') ){

                              return $first_menu;

                        }

                  }

            } else if( request()->is($menu) || request()->is($menu.'/*') ){

                  return true;

            }

            return false;

        }

    }

if(!function_exists('getromawi')){
      function getromawi($bln)
      {          

            switch ($bln){
                  case 1: 
                      return "I";
                      break;
                  case 2:
                      return "II";
                      break;
                  case 3:
                      return "III";
                      break;
                  case 4:
                      return "IV";
                      break;
                  case 5:
                      return "V";
                      break;
                  case 6:
                      return "VI";
                      break;
                  case 7:
                      return "VII";
                      break;
                  case 8:
                      return "VIII";
                      break;
                  case 9:
                      return "IX";
                      break;
                  case 10:
                      return "X";
                      break;
                  case 11:
                      return "XI";
                      break;
                  case 12:
                      return "XII";
                      break;
              }
      }

  }