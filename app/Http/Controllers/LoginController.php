<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Session;
use DB;
use Hash;
use Validator;
use Carbon\Carbon;

class LoginController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	 public function __construct()
     {
         $this->middleware('guest', ['except' => ['logout', 'login_as']]);
     }

    public function showLoginForm(Request $request)
    {		
        // if($request->session()->has('batas_waktu') && Carbon::now()<$request->session()->get('batas_waktu')) {
            
        //     $wkt_skrg = $request->session()->get('waktu_skrg');
        //     $batas_waktu = $request->session()->get('batas_waktu');
        //     $real_now = Carbon::now();
        //     $button_disabled = 1;
        //     return view('login', compact('button_disabled', 'batas_waktu', 'wkt_skrg', 'real_now'));
        // }else{
        //     if($request->session()->has('batas_waktu') || $request->session()->has('waktu_skrg')) {
        //         $request->session()->forget('batas_waktu');
        //         $request->session()->forget('waktu_skrg');
        //         $request->session()->forget('error_login');
        //     }

        //     if($request->session()->has('error_login')){
        //         $jumlah_error_login = $request->session()->get('error_login');
        //     }else{
        //         $jumlah_error_login = 0;
	    // }
			
//			$kelas = Kelas::get();
			
            return view('login');
        // }

    }
	
    public function logout(Request $request)
    {
        $this->guard('web')->logout();

		$request->session()->flush();
        $request->session()->regenerate();
        return redirect('/login');
    }

    public function login(Request $request)
    {		
		$this->validate($request, [
			'username'    => 'required',
			'password'   => 'required',
		]);
        
        $pass = $request->password;
        $ambil = User::where('username','=',$request->username)->first();        		
        
        if($ambil ){                        

            if($ambil->password == md5($pass)){
                $ambil_role =  DB::table('gate.sc_userrole')
									->join('gate.ms_unit', 'gate.ms_unit.kodeunit', '=', 'gate.sc_userrole.kodeunit')
                                    ->leftJoin('gate.sc_user', 'gate.sc_userrole.userid', '=', 'gate.sc_user.userid')
									->where('gate.sc_userrole.userid','=',$ambil->userid)
									->where('koderole','=','ESR')
									->first();
                if($ambil_role){
                        
                    Auth::login($ambil);
                    Session::put('username',$request->username);                    
                    // Session::put('name',$ambil->usr_nama);                    
                    //Session::put('login_as','user');
                    Session::put('subtitle',"UNIVERSITAS PGRI ADI BUANA SURABAYA");
                    Session::put('alamat',"Kampus Pusat : Jl. Dukuh Menanggal XII-4 Surabaya 60234 Telp. (031) 8281181");
                    Session::put('logoHeaderTransaksi',"img/logo_2.png");
                    Session::put('nama',$ambil_role->userdesc);
                    Session::put('idPgw',$ambil_role->idpegawai);
                    // Session::put('logoUHW',"img/UHW_5.png");


                    $ambil_role_admin =  DB::table('gate.sc_userrole')
                                        ->join('gate.ms_unit', 'gate.ms_unit.kodeunit', '=', 'gate.sc_userrole.kodeunit')
                                        ->leftJoin('gate.sc_user', 'gate.sc_userrole.userid', '=', 'gate.sc_user.userid')
                                        ->where('gate.sc_userrole.userid','=',$ambil->userid)
                                        ->where('koderole','=','ESRA')
                                        ->first();

                    if($ambil_role_admin){
                        Session::put('admin',"y");
                        $namaUnit = $ambil_role_admin->namaunit;
                        $kodeUnit = $ambil_role_admin->kodeunit;
                        $kodeRole = $ambil_role_admin->koderole;
                        $kodeSurat = $ambil_role_admin->kodesurat;
                    }else{
                        Session::put('admin',"n");
                        $namaUnit = $ambil_role->namaunit;
                        $kodeUnit = $ambil_role->kodeunit;
                        $kodeRole = $ambil_role->koderole;
                        $kodeSurat = $ambil_role->kodesurat;
                    }
                
                    Session::put('namaunit',$namaUnit);
                    Session::put('kodeunit',$kodeUnit);
                    Session::put('koderole',$kodeRole);
                    Session::put('kodesuratunit',$kodeSurat);

                    return response()->json([
                        'status'=>'insert_successful'
                    ]);

                }else{
                    return response()->json([
                        'status'=>'insert_tidak_akses',
                    ]);
                }

            }else{

                Session::put('username',$request->username);
                //return redirect()->back();
                return response()->json([
                    'status'=>'insert_failed_password',
                ]);
            }

        }else{
            return response()->json([
                    'status'=>'insert_failed_user',
                ]);
        }            

    }

	
    protected function guard()
    {
        return Auth::guard('web');
    }

}
