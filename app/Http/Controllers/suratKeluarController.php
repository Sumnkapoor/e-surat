<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\JenisSurat;
use App\Models\JenisSuratMasuk;
use App\Models\SuratKeluar;
use App\Models\SuratKeluarKirim;
use App\Models\SuratKeluarTeruskan;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Session;

class SuratKeluarController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'PROSES',
            'title' => 'SURAT KELUAR',
            'subtitle' => Session::get('subtitle'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_keluar/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }       

    public function add()
    {		
        $data = array(
            'head' => 'SURAT KELUAR',
            'title' => 'TAMBAH SURAT KELUAR',
            'alamatKampus' => Session::get('alamat'),
            'subtitle' => Session::get('subtitle'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        
        $LjenisSurat = JenisSurat::get();                
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);

        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_keluar/add',compact('data','LjenisSurat','LdataDepartement'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function edit(Request $request)
    {		
        $data = array(
            'head' => 'SURAT KELUAR',            
            'title' => 'EDIT SURAT KELUAR',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'act' => 'PUT',
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        

        $LjenisSurat = JenisSurat::get();                
        $LsuratKeluar = SuratKeluar::where('suratkeluarid','=',$request->id)->get();
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_keluar/add',compact('data','LjenisSurat','LdataDepartement','LsuratKeluar'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }    

    public function getData(Request $request)
    {
        $awal = date('n/d/Y', strtotime($request->awal));
        $akhir = date('n/d/Y', strtotime($request->akhir));
        $Ldata = SuratKeluar::get();         
        $kodeunit = Session::get('kodeunit');
        $status = Session::get('admin');
        
        if($status=='y') {

            $Ldata = DB::table('kesekretariatan.tr_seksuratkeluar as a')
            ->leftJoin('kesekretariatan.ms_jenissurat as b', 'a.idjenissurat', '=', 'b.jenissuratid')
            ->leftJoin('gate.ms_unit as c', 'a.kodeunitpembuat', '=', 'c.kodeunit')                        
            ->leftJoin(DB::raw("(SELECT count(idpegawai) as jumlahKirim, idsuratkeluar from kesekretariatan.tr_seksuratkeluarkirim group by idsuratkeluar) AS d"), 'a.suratkeluarid', '=', 'd.idsuratkeluar')
            ->leftJoin(DB::raw("(SELECT count(kodeunit) as jumlahTeruskan, idsuratkeluar from kesekretariatan.tr_seksuratkeluarteruskan group by idsuratkeluar) AS e"), 'a.suratkeluarid', '=', 'e.idsuratkeluar')
            ->select('a.*','b.*','c.kodeunit','c.namaunit', 'd.*', 'e.*')     
            ->whereBetween('suratkeluartanggal', [$awal,$akhir])
            ->orderBy('suratkeluartanggal','asc')
            ->get();            
            
        } else {

            $Ldata = DB::table('kesekretariatan.tr_seksuratkeluar as a')
            ->leftJoin('kesekretariatan.ms_jenissurat as b', 'a.idjenissurat', '=', 'b.jenissuratid')
            ->leftJoin('gate.ms_unit as c', 'a.kodeunitpembuat', '=', 'c.kodeunit')                        
            ->leftJoin(DB::raw("(SELECT count(idpegawai) as jumlahKirim, idsuratkeluar from kesekretariatan.tr_seksuratkeluarkirim group by idsuratkeluar) AS d"), 'a.suratkeluarid', '=', 'd.idsuratkeluar')
            ->leftJoin(DB::raw("(SELECT count(kodeunit) as jumlahTeruskan, idsuratkeluar from kesekretariatan.tr_seksuratkeluarteruskan group by idsuratkeluar) AS e"), 'a.suratkeluarid', '=', 'e.idsuratkeluar')
            ->select('a.*','b.*','c.kodeunit','c.namaunit', 'd.*', 'e.*')     
            ->where('a.kodeunitpembuat', '=', $kodeunit)
            ->whereBetween('suratkeluartanggal', [$awal,$akhir])
            ->orderBy('suratkeluartanggal','asc')
            ->get();

        }
                
        if($Ldata) {
            return response()->json([
                'status'=>'oke',
                'data' => $Ldata
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }        

    public function store(Request $request)
    {
        if($request->ajax()){

            $month = date("n");
            $year = date("Y");
            $kodeSurat = $request->kodeSurat;
            $kodeSuratUnit = Session::get('kodesuratunit');

            $orderObj = DB::table('kesekretariatan.tr_seksuratkeluar')
                        ->select('suratkeluarnomor')                        
                        ->latest('suratkeluarid')->first();   
            
            if ($orderObj) {
                $lastKodeNumber = explode('/',$orderObj->suratkeluarnomor);
                $lastKodeNumber2 = $lastKodeNumber[0];
                $KodeNumber2 = str_pad($lastKodeNumber2 + 1, 4, "0", STR_PAD_LEFT);
                
            } else {
                $KodeNumber2 = str_pad(1, 4, "0", STR_PAD_LEFT);                 
            }

            $noSurat = ($KodeNumber2.'/'.$kodeSurat.'/'.$kodeSuratUnit.'/'.getRomawi($month).'/'.$year);                                    
            
            DB::beginTransaction();
            try {
                $insert = SuratKeluar::create([
                    "idjenissurat"=> $request->jenisSurat,
                    "suratkeluartanggal"=> date("n/d/Y"),
                    "kodeunitpembuat"=> Session::get('kodeunit'),
                    "suratkeluarnomor"=> $noSurat,
                    "suratkeluarwho"=> Session::get('username'),
                    "suratkeluarwhen"=> date("n/d/Y H:i:s")
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json([
                        'status'=>'insert_successful',
                        'tgl'=> date("n/d/Y"),
                        'nomerSurat'=> $noSurat,
                        'idSuratKeluar'=> $insert->suratkeluarid
                    ]);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function updateContent(Request $request)
    {
        if($request->ajax()){

            $this->validate($request, [
                'file' => 'mimetypes:application/pdf'
            ]);

            $file = $request->file('file');            

            // membuat nama file unik
            $nama_file = ($request->file('file')) ? rand()."".$file->getClientOriginalName() : Null;            
            $path = ($request->file('file')) ? public_path('upload/surat_keluar/') : Null;

            // upload file
            if($request->file('file')) {
                $file->move('upload/surat_keluar',$nama_file);            
            } 

            DB::beginTransaction();
            try {
                $update = SuratKeluar::where('suratkeluarid', '=', $request->id_surat_keluar)->update([                
                    "suratkeluarkepada"=> $request->kepada,	                
                    "suratkeluarperihal"=> $request->perihal,
                    "suratkeluarisi"=> $request->isi,
                    "suratkeluarjudul"=> $request->judul,
                    "suratkeluarpath"=> $path,
                    "suratkeluarfilename"=> $nama_file,
                    "suratkeluarstatus"=> 'y',
                    "suratkeluarupdatewho"=> Session::get('username'),
                    "suratkeluarupdatewhen"=> date("Y-m-d H:i:s")
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $query = SuratKeluar::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }


    // Untuk proses Send Surat

    public function send(Request $request)
    {		
        $data = array(
            'head' => 'SURAT KELUAR',            
            'title' => 'KIRIM SURAT KELUAR',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'act' => 'PUT',
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
            'idsuratkeluar' => $request->id,
        );        
                
        // $LdataPegawai = DB::table('akademik.ms_pegawai')                
        //     ->select('nama','nik','idpegawai')     
        //     ->where('statuspeg','=','Aktif')
        //     ->orderBy('nama','asc')
        //     ->get();

        $LdataPegawai = DB::table('gate.sc_user')                
            ->select('userdesc','idpegawai')
            ->whereNotNull('idpegawai')     
            ->where('isactive','=','1')
            ->orderBy('userdesc','asc')
            ->get();

        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_keluar/send',compact('data','LdataPegawai'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }
    
    public function getDataSend(Request $request)
    {               
        $Ldata = DB::table('kesekretariatan.tr_seksuratkeluarkirim as a')
            ->leftJoin('gate.sc_user as b', 'a.idpegawai', '=', 'b.idpegawai')            
            ->select('a.*','b.userdesc','b.username')     
            ->where('a.idsuratkeluar','=',$request->id)        
            ->get();
        
        if($Ldata) {
            return response()->json([
                'status'=>'oke',
                'data' => $Ldata
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }

    public function storeSend(Request $request)
    {
        if($request->ajax()){      

            DB::beginTransaction();
            try {
                $insert = SuratKeluarKirim::create([
                    "idsuratkeluar"=> $request->idsuratkeluar,
                    "idpegawai"=> $request->kirim_pgw,
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function destroySend(Request $request, $id)
    {
        if($request->ajax()){
            $query = SuratKeluarKirim::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

    // Untuk proses Teruskan Surat
    public function forward(Request $request)
    {		
        $data = array(
            'head' => 'SURAT KELUAR',            
            'title' => 'FORWARD SURAT KELUAR',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'act' => 'PUT',
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
            'idsuratkeluar' => $request->id,
        );        
        
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_keluar/forward',compact('data','LdataDepartement'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function getDataTeruskan(Request $request)
    {               
        $Ldata = DB::table('kesekretariatan.tr_seksuratkeluarteruskan as a')
            ->leftJoin('gate.ms_unit as b', 'a.kodeunit', '=', 'b.kodeunit')            
            ->select('a.*','b.kodeunit','b.namaunit')     
            ->where('a.idsuratkeluar','=',$request->id)        
            ->get();
        
        if($Ldata) {
            return response()->json([
                'status'=>'oke',
                'data' => $Ldata
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }

    public function storeTeruskan(Request $request)
    {
        if($request->ajax()){            
            
            DB::beginTransaction();
            try {

                $insert = SuratKeluarTeruskan::create([
                    "idsuratkeluar"=> $request->idsuratkeluar,
                    "kodeunit"=> $request->dep_diteruskan,
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function destroyTeruskan(Request $request, $id)
    {
        if($request->ajax()){
            $query = SuratKeluarTeruskan::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

}
