<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\JenisSuratMasuk;
use App\Models\SuratMasuk;
use App\Models\Disposisi;
use App\Models\MasterDisposisi;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Session;

class SuratMasukController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'PROSES',
            'title' => 'SURAT MASUK',            
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_masuk/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }
    
    public function add()
    {		
        $data = array(
            'head' => 'SURAT MASUK',
            'title' => 'TAMBAH SURAT MASUK',
            'subtitle' => Session::get('subtitle'),
            'act' => 'POST',
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        

        $LjenisSuratMasuk = JenisSuratMasuk::get();                
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_masuk/add',compact('data','LjenisSuratMasuk','LdataDepartement'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function edit(Request $request)
    {		
        $data = array(
            'head' => 'SURAT MASUK',            
            'title' => 'EDIT SURAT MASUK',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'act' => 'PUT',
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        

        $LjenisSuratMasuk = JenisSuratMasuk::get();                
        $LsuratMasuk = SuratMasuk::where('suratmasukid','=',$request->id)->get();
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('surat_masuk/add',compact('data','LjenisSuratMasuk','LdataDepartement','LsuratMasuk'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function disposisi(Request $request)
    {		

        // Cari Nomer Disposisi
        $orderObj = DB::table('kesekretariatan.tr_seksuratdisposisi')
                        ->select('suratdisposisinomoragenda')                        
                        ->latest('suratdisposisiid')->first();   

        if ($orderObj) {
            $lastKodeNumber = $orderObj->suratdisposisinomoragenda;            
            $KodeNumber2 = str_pad($lastKodeNumber + 1, 4, "0", STR_PAD_LEFT);
            
        } else {
            $KodeNumber2 = str_pad(1, 4, "0", STR_PAD_LEFT);                 
        }

        $data = array(
            'head' => 'SURAT MASUK',
            'title' => 'LEMBAR DISPOSISI',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'id' => $request->id,
            'noDisposisi' => $KodeNumber2,
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );    

        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);

        // $LdataPegawai = DB::table('akademik.ms_pegawai')                
        //     ->select('nama','nik','idpegawai')     
        //     ->where('statuspeg','=','Aktif')
        //     ->orderBy('nama','asc')
        //     ->get();

        $LdataPegawai = DB::table('gate.sc_user')                
            ->select('userdesc','idpegawai')
            ->whereNotNull('idpegawai')     
            ->where('isactive','=','1')
            ->orderBy('userdesc','asc')
            ->get();

        $LMasterDisposisi = MasterDisposisi::get();

        if(($request->id) > 0){
            $dataDisposisi = DB::select (
                DB::raw('
                    select *
                    from kesekretariatan.tr_seksuratdisposisi 
                    where idsuratmasuk = '.$request->id.'
                ')
            );
            $returnHTML = view('surat_masuk/disposisi',compact('data','LMasterDisposisi','LdataDepartement','LdataPegawai','dataDisposisi'))->render();
        } else {
            $returnHTML = view('surat_masuk/disposisi',compact('data','LdataDepartement','LdataPegawai','LMasterDisposisi'))->render();
        }

        //return view('edit_perkiraan/index', compact('data'));        
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function getData(Request $request)
    {
        $awal = date('n/d/Y', strtotime($request->awal));
        $akhir = date('n/d/Y', strtotime($request->akhir));
        $Ldata = SuratMasuk::get();         

        $Ldata = DB::table('kesekretariatan.tr_seksuratmasuk as a')
            ->leftJoin('kesekretariatan.ms_jenissuratmasuk as b', 'a.idjenissuratmasuk', '=', 'b.jenissuratmasukid')
            ->leftJoin('gate.ms_unit as c', 'a.kodeunitpenerima', '=', 'c.kodeunit')            
            ->leftJoin('kesekretariatan.tr_seksuratdisposisi as d', 'a.suratmasukid', '=', 'd.idsuratmasuk')            
            ->select('a.*','b.*','c.kodeunit','c.namaunit','d.suratdisposisinosurat')     
            ->whereBetween('suratmasuktanggal', [$awal,$akhir])
            ->orderBy('suratmasuktanggal','asc')
            ->get();
        
        if($Ldata) {
            return response()->json([
                'status'=>'oke',
                'data' => $Ldata
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }
    
    private function validateRequest($request, $id=0){

        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];

        return Validator::make($request->all(), [
            "kode_perkiraan" => "required|unique:m_perkiraan,kode_perkiraan".($id ? ",".$id.",id" : "" ),
            "nama_perkiraan" => "required",			
        ], $messages);
    }

    public function store(Request $request)
    {
        if($request->ajax()){            

            $this->validate($request, [
                'file' => 'mimetypes:application/pdf'
            ]);
            
            $file = $request->file('file');        
            // membuat nama file unik
            $nama_file = ($request->file('file')) ? rand()."".$file->getClientOriginalName() : Null;            
            $path = ($request->file('file')) ? public_path('upload/surat_keluar/') : Null;            

            // upload file
            if($request->file('file')) {
                $file->move('upload/surat_masuk',$nama_file);
            }

            DB::beginTransaction();
            try {
                $insert = SuratMasuk::create([                
                    "suratmasukalamat"=> $request->alamat,								
                    "suratmasuknomor"=> $request->nomor_surat,
                    "suratmasukperihal"=> $request->perihal,
                    "idjenissuratmasuk"=> $request->jenis_surat_masuk,                
                    "suratmasuktanggal"=> $request->tgl,                
                    "suratmasukpath"=> $path,
                    "suratmasukfilename"=> $nama_file,                
                    "kodeunitpenerima"=> $request->departemen,
                    "suratmasukwho"=> Session::get('username'),
                    "suratmasukwhen"=> date("n/d/Y H:i:s")
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function store2(Request $request)
    {
        if($request->ajax()){

            DB::beginTransaction();
            try {         

                $insert = Disposisi::create([                
                    "suratdisposisinomoragenda"=> $request->nomer_disposisi,
                    "suratdisposisitglterima"=> $request->tgl_terima,
                    "suratdispossisitglsurat"=> $request->tgl_surat,                
                    "suratdisposisinosurat"=> $request->no_surat,
                    "suratdisposisisifat"=> $request->sifat,
                    "suratdisposisiperihal"=> $request->perihal,
                    "suratdisposisikepada"=> $request->kepada,
                    "suratdisposisiisi"=> $request->isi,
                    "suratdisposisidari"=> $request->dari,
                    "suratdisposisikembali"=> $request->dikembalikan,
                    "suratdisposisiditeruskan"=> $request->diteruskan,
                    "suratdisposisikembalitgl"=> $request->tgl_dikembalikan,
                    "suratdisposisiditeruskantgl"=> $request->tgl_diteruskan,                
                    "suratdisposisinamapengirim"=> $request->nama_pengirim,                
                    "suratdisposisisiapsimpantgl"=> $request->tgl_siap_simpan,                
                    "idsuratmasuk"=> $request->suratmasukid,                
                    "iddisposisi"=> $request->jenis_disposisi,                
                    "kodeunitditeruskan"=> $request->dep_diteruskan
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function update2(Request $request, $id)
    {
        if($request->ajax()){            

            DB::beginTransaction();
            try {    
                $update = Disposisi::where('suratdisposisiid', '=', $request->idTr)->update([            
                    "suratdisposisitglterima"=> $request->tgl_terima,
                    "suratdispossisitglsurat"=> $request->tgl_surat,                
                    "suratdisposisinosurat"=> $request->no_surat,
                    "suratdisposisisifat"=> $request->sifat,
                    "suratdisposisiperihal"=> $request->perihal,
                    "suratdisposisikepada"=> $request->kepada,
                    "suratdisposisiisi"=> $request->isi,
                    "suratdisposisidari"=> $request->dari,
                    "suratdisposisikembali"=> $request->dikembalikan,
                    "suratdisposisiditeruskan"=> $request->diteruskan,
                    "suratdisposisikembalitgl"=> $request->tgl_dikembalikan,
                    "suratdisposisiditeruskantgl"=> $request->tgl_diteruskan,                
                    "suratdisposisinamapengirim"=> $request->nama_pengirim,                
                    "suratdisposisisiapsimpantgl"=> $request->tgl_siap_simpan,                
                    "idsuratmasuk"=> $request->suratmasukid,                
                    "iddisposisi"=> $request->jenis_disposisi,                
                    "kodeunitditeruskan"=> $request->dep_diteruskan
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function update(Request $request, $id)
    {
        if($request->ajax()){

            DB::beginTransaction();
            try {  
            
                $update = SuratMasuk::where('suratmasukid', '=', $id)->update([
                    "suratmasukalamat"=> $request->alamat,								
                    "suratmasuknomor"=> $request->nomor_surat,
                    "suratmasukperihal"=> $request->perihal,
                    "idjenissuratmasuk"=> $request->jenis_surat_masuk,                
                    "suratmasuktanggal"=> $request->tgl,                
                    "kodeunitpenerima"=> $request->departemen,
                    "suratmasukupdatewho"=> Session::get('username'),
                    "suratmasukupdatewhen"=> date("n/d/Y H:i:s")
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $query = SuratMasuk::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

}
