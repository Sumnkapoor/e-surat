<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Departement;
use Auth;
use Session;

class DepartementController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'SETUP',
            'title' => 'DEPARTEMENT',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnAdd' => 'Tambah',
            'btnClassPic' => 'btn btn-outline-success btn-sm btn-pic px-2 ms-2',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        
        //return view('edit_perkiraan/index', compact('data'));        

        $returnHTML = view('departement/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }       

    public function pic()
    {		
        $data = array(
            'head' => 'DEPARTEMENT',
            'title' => 'PIC Departement',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('departement/pic',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function getData()
    {        
        $dataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);

        if($dataDepartement) {
            return response()->json([
                'status'=>'oke',
                'data' => $dataDepartement
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }
    
    private function validateRequest($request, $id=0){

        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];

        return Validator::make($request->all(), [
            "kode_perkiraan" => "required|unique:m_perkiraan,kode_perkiraan".($id ? ",".$id.",id" : "" ),
            "nama_perkiraan" => "required",			
        ], $messages);
    }

    public function store(Request $request)
    {
        if($request->ajax()){
            
            DB::beginTransaction();
            try {
                $insertPerkiraan = EditPerkiraan::create([
                    "kode_perkiraan"=> $request->kode_perkiraan,
                    "nama_perkiraan"=> $request->nama_perkiraan,								
                    "user_record"=> $request->nama_perkiraan,
                    "df_trans_perkiraan"=> $request->keterangan,
                    // "user_record"=> Auth::user()->name,
                    "dt_record"=> date("Y-m-d H:i:s")
                ]);

                if($insertPerkiraan) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }

        } else {
            return redirect('asset/');
        }

    }

    public function update(Request $request, $id)
    {
        if($request->ajax()){
            
            DB::beginTransaction();
            try {
                $update = Departement::where('kodeunit', '=', $id)->update([
                    "kodesurat"=> $request->kode_surat,
                    "unitsurat"=> $request->unit_surat                
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }   
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $query = EditPerkiraan::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

}
