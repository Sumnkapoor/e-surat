<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\JenisSurat;
use App\Models\DetailSurat;
use Auth;
use Session;

class JenisSuratController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'SETUP',
            'title' => 'JENIS SURAT',
            'subtitle' => Session::get('subtitle'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('jenis_surat/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }       

    public function getData()
    {

        $data = DB::table('kesekretariatan.ms_jenissurat as a')
            ->leftJoin('kesekretariatan.ms_jenissuratdetail as b', 'a.jenissuratid', '=', 'b.idjenissurat')            
            ->select('*')                        
            ->get();            

        if($data) {
            return response()->json([
                'status'=>'oke',
                'data' => $data
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }
    
    public function searchDetailSurat(Request $request)
    {
        $data = DetailSurat::where('idjenissurat', '=', $request->id)->get();

        if(count($data)>0) {
            return response()->json([
                'status'=>'PUT',
                'data' => $data
                ]);
        } else {
            return response()->json([
                'status'=>'POST',
                'data' => $data
                ]);
        }

    }

    public function store(Request $request)
    {
        if($request->ajax()){

            DB::beginTransaction();
            try {
                $insert = JenisSurat::create([
                    "jenissuratnama"=> $request->nama_surat,
                    "jenissuratkode"=> $request->kode_surat                
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function store2(Request $request)
    {
        if($request->ajax()){

            DB::beginTransaction();
            try {   
                $insert = DetailSurat::create([
                    "jenissuratdetailperihal"=> $request->perihal,
                    "jenissuratdetailkepada"=> $request->kepada,
                    "jenissuratdetailjudul"=> $request->judul,
                    "jenissuratdetailisi"=> $request->isi,
                    "idjenissurat"=> $request->id_jenis_surat
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return redirect('asset/');
        }

    }

    public function update(Request $request, $id)
    {
        if($request->ajax()){    

            DB::beginTransaction();
            try {   
                $update = JenisSurat::where('jenissuratid', '=', $id)->update([
                    "jenissuratnama"=> $request->nama_surat,
                    "jenissuratkode"=> $request->kode_surat
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function update2(Request $request, $id)
    {
        if($request->ajax()){
            
            DB::beginTransaction();
            try {   
                $update = DetailSurat::where('idjenissurat', '=', $id)->update([
                    "jenissuratdetailperihal"=> $request->perihal,
                    "jenissuratdetailkepada"=> $request->kepada,
                    "jenissuratdetailjudul"=> $request->judul,
                    "jenissuratdetailisi"=> $request->isi,                
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            }   
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $query = JenisSurat::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

    private function validateRequest($request, $id=0){

        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];

        return Validator::make($request->all(), [
            "nama_surat" => "required",			
            "kode_surat" => "required",			
        ], $messages);
    }

}
