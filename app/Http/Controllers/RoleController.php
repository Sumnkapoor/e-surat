<?phpnamespace App\Http\Controllers;
use Illuminate\Http\Request;use App\Models\Role;use App\Models\Modul;use App\Models\RoleModul;use Illuminate\Support\Facades\Validator;use DB;
class RoleController extends Controller{

    public function index()    {		$data = array(            'title' => 'Hak Akses Setiap ROle',            'btnAdd' => 'Tambah',        );    
        $moduls = Modul::get();
        return view('role/index', compact('moduls','data'));
    }
    public function getData()    {        // return 123;        // $role = Role::with('moduls')->get();
        // return 123;        
        $role = Role::with([
            'moduls' => function($q) {
                $q->join('m_modul as m','m.modul_id','=','m_roles_moduls.modul_id')
                ->select('m_roles_moduls.*', 'm.nama_modul');
            }
        ])->where('role_id','!=',0)->get();
        
        // $role = Role::join('m_roles_moduls as rm','m_role.role_id','=','rm.role_id')
        // ->join('m_modul as m','m.modul_id','=','rm.modul_id')
        // ->select('m_role.*','m.role_id','m.nama_modul')->get();

        if($role) {
            return response()->json([
                'status'=>'oke',
                'data' => $role
            ]);
        } else {
            return response()->json(['status'=>'insert_failed']);
        }
    } 
    
    
   
    private function validateRequest($request, $id=0){
        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];
        return Validator::make($request->all(), [
            "nama_role" => "required|unique:m_role,nama_role".($id ? ",".$id.",role_id" : "" )
        ], $messages);
    }

     
    public function store(Request $request)
    {
        if($request->ajax()){
            if ($this->validateRequest($request)->fails()) {

                return response()->json([
                    'status'=>'insert_failed',
                    'error' => $this->validateRequest($request)->messages()
                ]);
                
            }
            DB::beginTransaction();

            try {
                $insertRole = DB::table('m_role')->insertGetId([
                    "nama_role" => $request->nama_role,                    "dt_record" => date("Y-m-d H:i:s"),                    "user_record"=> 1,					"dt_modified" => date("Y-m-d H:i:s"),					"user_modified"=> 1
                ]);				
                foreach ($request->modul as $key => $idModul) {
                    $user_modul = DB::table('m_roles_moduls')->insert([
                        "role_id" => $insertRole,
                        "modul_id" => $idModul
                    ]);
                }
                DB::commit();
                return response()->json(['status'=>'insert_successful']);

            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['status'=>'insert_failed']);
            }
        } else {
            return redirect('/');
        }
    }    
    public function show(Request $request,$id)    {
        if($request->ajax()){            $modul = DB::table('m_roles_moduls')->where('role_id',$id)->get();            if($modul) {                return response()->json([                    'status'=>'oke',                    'data' => $modul                ]);
            } else {
                return response()->json(['status'=>'insert_failed']);
            }
        } else {
            return redirect('/');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function update(Request $request, $id)
    {
        // return $request->all();        if($request->ajax()){
            if ($this->validateRequest($request, $id)->fails()) {
                return response()->json([                    'status'=>'insert_failed',                    'error' => $this->validateRequest($request, $id)->fails()                ]);

            }
            DB::beginTransaction();
            try {
                $insertRole = Role::find($id)->update([
                    "nama_role" => $request->nama_role,
                    "dt_modified" => date("Y-m-d H:i:s"),
                    "user_modified"=> 1
                ]);
                $moduls = RoleModul::where('role_id', $id)->delete();
                foreach ($request->modul as $key => $idModul) {                    $user_modul = DB::table('m_roles_moduls')->insert([                        "role_id" => $id,                        "modul_id" => $idModul                    ]);                }
                DB::commit();
                return response()->json(['status'=>'insert_successful']);
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['status'=>'insert_failed']);

            }

            // $updateRole = Role::find($id)->update([
            //     "nama_role" => $request->nama_role,
            //     "dt_modified" => date("Y-m-d H:i:s"),
            //     "user_modified"=> 1
            // ]);

            if($updateRole) {                return response()->json(['status'=>'insert_successful']);            } else {                return response()->json(['status'=>'insert_failed']);            }
        } else {            return redirect('/');        }
    }
    public function destroy($id)
    {		DB::table('m_roles_moduls')->where('role_id', '=', $id)->delete();
        $query = Role::find($id)->delete();
        if($query) {            return response()->json(['status'=>'delete_successful']);        } else {            return response()->json(['status'=>'delete_failed']);        }
    }
    public function deleteData(Request $request, $id)    {
        if($request->ajax()){            $query = Role::find($id)->delete();            $modul = DB::table('m_roles_moduls')->where('role_id',$id)->delete();            if($query) {                return response()->json(['status'=>'delete_successful']);            } else {                return response()->json(['status'=>'delete_failed']);            }
        } else {
            return redirect('/');
        }
    }
}