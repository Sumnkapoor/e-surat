<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\JenisSuratMasuk;
use Auth;
use Session;

class JenisSuratMasukController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'SETUP',
            'title' => 'JENIS SURAT MASUK',
            'subtitle' => Session::get('subtitle'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormSelect2' => 'single-select',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('jenis_surat_masuk/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }       

    public function getData()
    {
        $data = JenisSuratMasuk::get();

        if($data) {
            return response()->json([
                'status'=>'oke',
                'data' => $data
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }
    
    private function validateRequest($request, $id=0){

        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];

        return Validator::make($request->all(), [            
            "nama_surat" => "required",			
        ], $messages);
    }

    public function store(Request $request)
    {
        if($request->ajax()){    

            DB::beginTransaction();
            try {   
                $insert = JenisSuratMasuk::create([
                    "jenissuratmasuknama"=> $request->nama_surat,            
                ]);

                if($insert) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            } 
        } else {
            return redirect('asset/');
        }

    }

    public function update(Request $request, $id)
    {
        if($request->ajax()){
            
            DB::beginTransaction();
            try {   
                $update = JenisSuratMasuk::where('jenissuratmasukid', '=', $id)->update([
                    "jenissuratmasuknama"=> $request->nama_surat,                            
                ]);

                if($update) {
                    DB::commit();
                    return response()->json(['status'=>'insert_successful']);
                } else {
                    return response()->json(['status'=>'insert_failed']);
                }
            } catch (\Throwable $e) {

                DB::rollback();            
                throw $e;            
                return response()->json(['status'=>'insert_failed']);

            } 
        } else {
            return response()->json(['status'=>'proses_failed']);
        }

    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $query = JenisSuratMasuk::find($id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return response()->json(['status'=>'delete_failed']);
        }
    }

}
