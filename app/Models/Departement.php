<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    protected $table = 'gate.ms_unit';

    protected $primaryKey = 'kodeunit';

    public $timestamps = false;
    protected $fillable = [
        'kodesurat',
        'unitsurat',		
    ];	   
    
}
