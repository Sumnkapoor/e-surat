<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSurat extends Model
{
    protected $table = 'kesekretariatan.ms_jenissuratdetail';

    protected $primaryKey = 'jenissuratdetailid';

    public $timestamps = false;
    protected $fillable = [
        'idjenissurat',
        'jenissuratdetailperihal',
		'jenissuratdetailkepada',
        'jenissuratdetailjudul',
        'jenissuratdetailisi'
    ];	   
    
}
