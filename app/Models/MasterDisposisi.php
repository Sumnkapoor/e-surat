<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDisposisi extends Model
{
    protected $table = 'kesekretariatan.ms_isidisposisi';

    protected $primaryKey = 'disposisiid';

    public $timestamps = false;
    protected $fillable = [
        'disposisinama'        
    ];	   
    
}
