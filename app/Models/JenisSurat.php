<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisSurat extends Model
{
    protected $table = 'kesekretariatan.ms_jenissurat';

    protected $primaryKey = 'jenissuratid';

    public $timestamps = false;
    protected $fillable = [
        'jenissuratnama',
		'jenissuratkode'		
    ];	   
    
}
