<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disposisi extends Model
{
    protected $table = 'kesekretariatan.tr_seksuratdisposisi';

    protected $primaryKey = 'suratdisposisiid';

    public $timestamps = false;
    protected $fillable = [
        'suratdisposisiid',
        'suratdisposisinomoragenda',
        'suratdisposisitglterima',
        'suratdispossisitglsurat',
        'suratdisposisinosurat',
        'suratdisposisiperihal',
        'suratdisposisiisi',
        'suratdisposisikembali',
        'suratdisposisiditeruskan',
        'suratdisposisikembalitgl',
        'suratdisposisiditeruskantgl',
        'suratdisposisikodeklasifikasi',
        'suratdisposisinamapengirim',
        'suratdisposisisiapsimpan',
        'suratdisposisisiapsimpantgl',
        'kodeunitditeruskan',
        'suratdisposisianggaranunit',
        'suratdisposisianggarantugas',
        'suratdisposisisifat',
        'idsuratmasuk',
        'iddisposisi',
        'suratdisposisikepada',
        'suratdisposisidari'
    ];	   
    
}
