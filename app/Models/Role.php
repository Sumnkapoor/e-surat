<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;use App\Models\Modul;
class Role extends Model
{    protected $table = 'm_role';    protected $primaryKey = 'role_id';    public $timestamps = false;    protected $fillable = [        "role_id",        "nama_role",        "dt_record",        "dt_modified",        "user_record",        "user_modified"    ];
    public function moduls()    {      return $this->hasMany(RoleModul::class,'role_id');    }
}
