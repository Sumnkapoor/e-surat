<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RoleModul extends Model
{
    protected $table = 'm_roles_moduls';

    public $timestamps = false;

    protected $fillable = [
        "role_id",
        "modul_id"
    ];
    
    public function modul()
    {
        return $this->belongsTo(Modul::class,'modul_id');   
    }

}
