<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratKeluar extends Model
{
    protected $table = 'kesekretariatan.tr_seksuratkeluar';

    protected $primaryKey = 'suratkeluarid';

    public $timestamps = false;
    protected $fillable = [        
        'idjenissurat',		
        'suratkeluarkepada',
        'suratkeluarperihal',
        'suratkeluartanggal',
        'suratkeluarisi',        
        'suratkeluarjudul',        
        'kodeunitpembuat',        
        'suratkeluarnomor',
        'suratkeluarwho',
        'suratkeluarwhen',
        'suratkeluararsip',
        'suratkeluarstatus',
        'suratkeluarpath',
        'suratkeluarfilename',
        'suratkeluarupdatewho',
        'suratkeluarupdatewhen',
    ];	   
    
}
