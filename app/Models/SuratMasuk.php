<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratMasuk extends Model
{
    protected $table = 'kesekretariatan.tr_seksuratmasuk';

    protected $primaryKey = 'suratmasukid';

    public $timestamps = false;
    protected $fillable = [        
        'suratmasukalamat',
		'suratmasuknomor',
        'suratmasukperihal',
        'idjenissuratmasuk',
        'idsuratdisposisi',
        'suratmasuktanggal',        
        'suratmasukpath',        
        'suratmasukfilename',        
        'kodeunitpenerima',
        'suratmasukwho',
        'suratmasukwhen',
        'suratmasukupdatewho',
        'suratmasukupdatewhen'
    ];	   
    
}
