<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-warning">
        <div class="card-header bg-primary py-3">        
            <div class="row">
                <div class="col-md-1" >
                    <img src="{{ URL::asset(session("logoHeaderTransaksi")) }}" width="100px" alt="" />                                       
                </div>
                <div class="col-md-11">
                    <br>
                    <h2 class="text-white">{{$data['subtitle']}}</h2>
                    <h6 class="text-white">{{$data['alamatKampus']}}</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">            
                    <div class="invoice">                    
                        <form enctype="multipart/form-data" class="form-horizontal form-label-left" id="form_upload" method="post">
                            @csrf                                                    
                            <input type="hidden" class="form-control" id="method_field" name="_method" value="{{$data['act']}}" />
                            <input type="hidden" class="{{$data['classFormControl']}}" id="idTr" value="{{$LsuratMasuk[0]->suratmasukid ?? ''}}" name="idTr">                                                            
                            <div class="row form_custom"> 
                                <div class="col-sm-12">                                                                                                                        
                                    <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Alamat Pengirim</label>
                                    <input type="text" class="{{$data['classFormControl']}}" id="alamat" value="{{$LsuratMasuk[0]->suratmasukalamat ?? ''}}" name="alamat">
                                    <label for="alamat" generated="true" class="error"></label>
                                    <label id="validationError"></label>                                            
                                    <div class="row">                                                                                         
                                        <div class="col-md-6">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal</label>
                                            <input type="text" class="form-control datepicker" id="tgl" value="{{$LsuratMasuk[0]->suratmasuktanggal ?? ''}}" name="tgl" >                                                
                                            <label for="tgl" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Nomor Surat</label>
                                            <input type="text" class="{{$data['classFormControl']}}" id="nomor_surat" value="{{$LsuratMasuk[0]->suratmasuknomor ?? ''}}" name="nomor_surat">
                                            <label for="nomor_surat" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>
                                    </div>
                                    <div class="row">                                                                                         
                                        <div class="col-md-6">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Jenis Surat Masuk</label>
                                            @if (!$LjenisSuratMasuk)
                                            <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                                <div class="text-white">Master Jenis Surat Masuk Belum Ada</div>
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                            </div>
                                            @else
                                            <select class="{{$data['classFormSelect2']}}" name="jenis_surat_masuk" id="jenis_surat_masuk" width="100%">
                                                <option value="">[Pilih Jenis Surat Masuk]</option>
                                                @foreach($LjenisSuratMasuk as $jenisSuratMasuk)
                                                <option value="{{ $jenisSuratMasuk->jenissuratmasukid }}" {{($jenisSuratMasuk->jenissuratmasukid == ($LsuratMasuk[0]->idjenissuratmasuk ?? '')) ? 'selected' : '' }}>{{ ucwords(strtolower($jenisSuratMasuk->jenissuratmasuknama)) }}</option>
                                                @endforeach                                                                                                    
                                            </select>                        
                                            @endif
                                            <label for="jenis_surat_masuk" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Departemen Penerima</label>
                                            @if (!$LdataDepartement)
                                            <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                                <div class="text-white">Master Departement Belum Ada</div>
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                            </div>
                                            @else
                                            <select class="{{$data['classFormSelect2']}}" name="departemen" id="departemen" width="100%">
                                                <option value="">[Pilih Departement Penerima]</option>
                                                @foreach($LdataDepartement as $dataDepartement)
                                                <option value="{{ $dataDepartement->kodeunit }}" {{($dataDepartement->kodeunit == ($LsuratMasuk[0]->kodeunitpenerima ?? '')) ? 'selected' : '' }}>{{ ucwords(strtolower($dataDepartement->namaunit)) }}</option>
                                                @endforeach                                                                                                    
                                            </select>                        
                                            @endif
                                            <label for="departemen" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>
                                    </div>
                                    <div class="row">                                                                                                                                         
                                        <div class="col-md-12">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Upload Surat</label>                                                    
                                                <div class="input-group">
                                                    <button class="btn btn-primary btn-sm" type="button" id="inputGroupFileAddon03">.pdf (Maks Size 2 Mb)</button>
                                                    <input type="file" name="file" class="form-control form-control-sm" value="" id="file">                                                        
                                                </div>		                                    												                                                    
                                            <label for="file" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>
                                    </div>
                                    <div class="row">                                                                                         
                                        <div class="col-md-12">
                                            <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Perihal</label>
                                            <textarea class="{{$data['classFormControl']}}" id="perihal" name="perihal" rows="8" cols="80">{{$LsuratMasuk[0]->suratmasukperihal ?? ''}}</textarea>
                                            <!-- <div id="editor-container"></div> -->
                                            <label for="perihal" generated="true" class="error"></label>
                                            <label id="validationError"></label>
                                        </div>                                                
                                    </div><br>
                                    <div class="row">                                                                                                                                         
                                        <div class="col-md-12" align="left">                                                                                                        
                                            <button type="submit" id="btn_simpan" class="btn btn-success btn-sm"><i class='bx bx-save mr-1'></i>Simpan</button>
                                            <!-- <button type="button" id="btn_kembali" class="btn btn-danger btn-sm"><i class='bx bx-trash mr-1'></i>Kembali</button> -->
                                        </div>
                                    </div>
                                </div>        
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    

<script src="{{ asset('additional/js/add_surat_masuk.js') }}"></script>
