<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-primary">
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">  
                    <div class="invoice">          
                        <div class="table-responsive">
                            <button id="tambah" class="{{$data['btnClass']}}">{{$data['btnAdd']}}</button><br><br>
                            <table id="example2" class="table table-striped table-bordered" border="2">
                    
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-form" id="exampleLargeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="modal_label">Form Tambah Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form-horizontal form-label-left" id="form" method="post">
                    <div class="modal-body">                
                        @csrf
                        <input type="hidden" class="form-control" id="method_field" name="_method" value="POST" />
                        <input type="hidden" class="form-control" id="id" value="" name="id">
                        <div id="error-validation"></div>
                        <div class="row g-2">
                            <div class="col-12">
                                <label for="inputPhoneNo" class="form-label"><b>Nama Surat</b></label>
                                <div class="input-group"> <span class="input-group-text bg-transparent"><i class='bx bxs-plus-square' ></i></span>
                                    <input type="text" class="form-control border-start-0" id="nama_surat" name="nama_surat" placeholder="Nama Surat" />
                                </div>
                                <label for="nama_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>
                            <div class="col-12">
                                <label for="inputEmailAddress" class="form-label"><b>Kode Surat</b></label>
                                <div class="input-group"> <span class="input-group-text bg-transparent"><i class='bx bxs-message' ></i></span>
                                    <input type="text" class="form-control border-start-0" id="kode_surat" name="kode_surat" placeholder="Kode Surat" />
                                </div>
                                <label for="kode_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>                        
                        </div>
                        
                    </div>
                    <div class="modal-footer">                    
                        <button type="button" id="btn_simpan" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Detail-->
    <div class="modal fade modal-form-detail" id="exampleLargeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="modal_label">Surat Berita Acara</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form-horizontal form-label-left" id="form2" method="post">
                    <div class="modal-body">                
                        @csrf                                                            
                        <input type="hidden" class="form-control" id="method_field_detail" name="_method_detail" value="POST" />
                        <input type="hidden" class="form-control" id="id_jenis_surat" value="" name="id_jenis_surat">
                        <div id="error-validation"></div>
                        <div class="row g-2">
                            <div class="col-12">
                                <label for="inputPhoneNo" class="form-label"><h6><b>Detail Surat :</b></h6></label>                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="y" name="perihal" id="perihal">
                                    <label class="form-check-label" for="flexCheckDefault">Perihal</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="y" name="kepada" id="kepada">
                                    <label class="form-check-label" for="flexCheckDefault">Kepada</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="y" name="judul" id="judul">
                                    <label class="form-check-label" for="flexCheckDefault">Judul</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="y" name="isi" id="isi">
                                    <label class="form-check-label" for="flexCheckDefault">Isi</label>
                                </div>                                                                                    
                            </div>                        
                        </div>
                        
                    </div>
                    <div class="modal-footer">                    
                        <button type="button" id="btn_simpan2" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('additional/js/jenis_surat.js') }}"></script>


