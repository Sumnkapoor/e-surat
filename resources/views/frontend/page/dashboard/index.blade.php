@extends('frontend.layout_home.default')

@push('style')

      <!-- Aditional Style CSS Here -->

@endpush


@section('content')

<div id="transContent">
      <h6 class="mb-0 text-uppercase"><b>SELAMAT DATANG</b> {{ Session::get('namaunit') }} </h6>
      <hr/>

      <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
            <div class="col">
                  <div class="card radius-10 bg-success bg-gradient">
                        <div class="card-body">
                              <div class="d-flex align-items-center">
                                    <div>
                                          <p class="mb-0 text-white">TOTAL SURAT MASUK </p>
                                          <h4 class="my-1 text-white">{{ $LdataSuratMasuk ?? 0}} Surat</h4>
                                          <p class="mb-0 font-13 text-white"><i class='bx bxs-up-arrow align-middle'></i></p>
                                    </div>
                                    <div class="text-white ms-auto font-35"><i class='bx bxs-envelope'></i>
                                    </div>
                              </div>                        
                        </div>
                  </div>
            </div>
            <div class="col">
                  <div class="card radius-10 bg-primary bg-gradient">
                        <div class="card-body">
                              <div class="d-flex align-items-center">
                                    <div>
                                          <p class="mb-0 text-white">TOTAL SURAT KELUAR</p>
                                          <h4 class="my-1 text-white">{{ $LdataSuratKeluar ?? 0}} Surat</h4>
                                          <p class="mb-0 font-13 text-white"><i class='bx bxs-up-arrow align-middle'></i></p>
                                    </div>
                                    <div class="text-white ms-auto font-35"><i class='bx bxs-envelope-open'></i>
                                    </div>
                              </div>                        
                        </div>
                  </div>
            </div>
            <div class="col-lg-12">
                  <div class="card radius-10 bg-danger bg-gradient">
                        <div class="card-body">
                              <div class="d-flex align-items-center">
                                    <div>
                                          <p class="mb-0 text-white">SURAT MASUK BELUM DISPOSISI</p>
                                          <h4 class="my-1 text-white">{{ $LdataBlmDisposisi ?? 0}} Surat</h4>
                                          <p class="mb-0 font-13 text-white"><i class='bx bxs-up-arrow align-middle'></i></p>
                                    </div>
                                    <div class="text-white ms-auto font-35"><i class='bx bxs-message-square-error'></i>
                                    </div>
                              </div>                        
                        </div>
                  </div>
            </div>
      </div>
</div>
@endsection

@push('scripts')	

@endpush
