<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-primary">
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">  
                    <div class="invoice">          
                        <div class="row">                                                                                         
                            <div class="col-md-6">
                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal Awal</label>
                                <input type="text" class="form-control datepicker" id="tgl_awal" value="" name="tgl_awal" >
                                <label for="tgl_awal" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>
                            <div class="col-md-6">
                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal Akhir</label>
                                <input type="text" class="form-control datepicker" id="tgl_akhir" value="" name="tgl_akhir" >
                                <label for="tgl_akhir" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>
                            <button id="btnCari" class="{{$data['btnClass']}}">Cari Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card border-top border-0 border-4 border-primary">
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">  
                    <div class="invoice">          
                        <!-- <div class="table-responsive"> -->
                            <!-- <button id="tambah" class="{{$data['btnClass']}}">{{$data['btnAdd']}}</button><br><br> -->
                            <a class="{{$data['btnClass']}} actionx" data-href="/addSuratKeluar">{{$data['btnAdd']}}</a><br><br>
                            <table id="example2" class="table table-striped table-bordered" border="2">                            
                            </table>                        
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('additional/js/surat_keluar.js') }}"></script>


