<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-warning">
        <div class="card-header bg-primary py-3">        
            <div class="row">
                <div class="col-md-1" >
                    <img src="{{ URL::asset(session("logoHeaderTransaksi")) }}" width="100px" alt="" />                                       
                </div>
                <div class="col-md-11">
                    <br>
                    <h2 class="text-white">{{$data['subtitle']}}</h2>
                    <h6 class="text-white">{{$data['alamatKampus']}}</h6>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">            
                    <div class="invoice">
                        <button id="tambah" class="{{$data['btnClass']}}">{{$data['btnAdd']}}</button><br><br>
                        <input type="hidden" class="form-control" id="idsuratkeluar" value="{{$data['idsuratkeluar']}}" name="idsuratkeluar">
                        <table id="example2" class="table table-striped table-bordered" border="2">                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-form" id="exampleLargeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_label">SURAT DITERUSKAN KE : </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form-horizontal form-label-left" id="form" method="post">
                    <div class="modal-body">                
                        @csrf
                        <input type="hidden" class="form-control" id="method_field" name="_method" value="POST" />                    
                        <div id="error-validation"></div>
                        <div class="row g-2">
                            <div class="col-12">
                                <label for="inputPhoneNo" class="form-label"><b>Nama Departement</b></label>                            
                                @if (!$LdataDepartement)
                                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                    <div class="text-white">Master Departement Belum Ada</div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                                @else
                                <select class="{{$data['classFormSelect2']}}" name="dep_diteruskan" id="dep_diteruskan" width="100%">
                                    <option value="">[Pilih Departement]</option>
                                    @foreach($LdataDepartement as $dataDepartement)
                                    <option value="{{ $dataDepartement->kodeunit }}" {{($dataDepartement->kodeunit == ($dataDisposisi[0]->kodeunitditeruskan ?? '')) ? 'selected' : '' }}>{{ ($dataDepartement->namaunit) }}</option>
                                    @endforeach                                                                                                    
                                </select>
                                @endif
                                <label for="dep_diteruskan" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>                        
                        </div>
                        
                    </div>
                    <div class="modal-footer">                    
                        <button type="button" id="btn_simpan" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('additional/js/surat_keluar_forward.js') }}"></script>
