<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-warning">
        <div class="card-header bg-primary py-3">        
            <div class="row">
                <div class="col-md-1" >
                    <img src="{{ URL::asset(session("logoHeaderTransaksi")) }}" width="100px" alt="" />                                       
                </div>
                <div class="col-md-11">
                    <br>
                    <h2 class="text-white">{{$data['subtitle']}}</h2>
                    <h6 class="text-white">{{$data['alamatKampus']}}</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div class="row">
                    <form class="form-horizontal form-label-left form_custom" id="formHeader" method="post">    
                        <div class="row">
                            <div class="col-md-6">
                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Jenis Surat</label>
                                @if (!$LjenisSurat)
                                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                    <div class="text-white">Master Jenis Surat Belum Ada</div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                                @else
                                <select class="{{$data['classFormSelect2']}}" name="jenis_surat" id="jenis_surat" width="100%">
                                    <option value="">[Pilih Jenis Surat]</option>
                                    @foreach($LjenisSurat as $jenisSurat)
                                    <option value="{{ $jenisSurat->jenissuratid }}" data-kode="{{ $jenisSurat->jenissuratkode }}" {{($jenisSurat->jenissuratid == ($LsuratKeluar[0]->idjenissurat ?? '')) ? 'selected' : '' }}>{{ ucwords(strtolower($jenisSurat->jenissuratnama)) }}</option>
                                    @endforeach                                                                                                    
                                </select>                                                                
                                @endif
                                <label for="jenis_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>          
                            <div class="col-md-6">
                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal Surat</label>
                                <input type="text" class="form-control" id="tgl_surat" name="tgl_surat" value="{{$LsuratKeluar[0]->suratkeluartanggal ?? ''}}" readonly/>
                                <input type="hidden" class="form-control" id="method_field_header" name="_method_header" value="POST" />
                                <label for="tgl_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>          
                        </div>
                        <div class="row">
                            <div class="col-md-12" align="left">                                                                                                        
                                <button type="button" id="btn_generate" class="btn btn-success btn-sm"><i class='bx bx-credit-card-front mr-1'></i>Generate No. Surat</button>
                            </div>
                        </div>
                    </form>    
                </div>
            </div><br>

            <div class="border border-primary p-3 rounded">
                <div id="invoice">            
                    <div class="invoice">                                        
                        <form enctype="multipart/form-data" class="form-horizontal form-label-left form_custom1" id="formContent" method="post">    
                            @csrf                                                                            
                            <div class="row">                                 
                                <div class="row">                                                                                         
                                    <div class="col-md-12">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">No. Surat</label>
                                        <input type="text" class="form-control" id="no_surat" name="no_surat" value="{{$LsuratKeluar[0]->suratkeluarnomor ?? ''}}" readonly/>
                                        <input type="hidden" class="form-control" id="method_field_content" name="_method_content" value="POST" />
                                        <input type="hidden" class="form-control" id="id_surat_keluar" name="id_surat_keluar" value="{{$LsuratKeluar[0]->suratkeluarid ?? ''}}" readonly/>
                                        <label for="no_surat" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>                                                
                                </div>
                                <div class="row">                                                                                         
                                    <div class="col-md-6">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Judul</label>
                                        <textarea class="form-control" id="judul" name="judul" placeholder="Judul..." rows="3">{{$LsuratKeluar[0]->suratkeluarjudul ?? ''}}</textarea>
                                        <label for="judul" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>                                                
                                    <div class="col-md-6">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Kepada</label>
                                        <textarea class="form-control" id="kepada" name="kepada" placeholder="Kepada..." rows="3">{{$LsuratKeluar[0]->suratkeluarkepada ?? ''}}</textarea>
                                        <label for="kepada" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>                                                
                                </div>
                                <div class="row">                                                                                         
                                    <div class="col-md-12">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Perihal</label>
                                        <textarea class="form-control" id="perihal" name="perihal" placeholder="Perihal..." rows="3">{{$LsuratKeluar[0]->suratkeluarperihal ?? ''}}</textarea>
                                        <label for="perihal" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>                                                
                                </div>
                                <div class="row">                                                                                         
                                    <div class="col-md-12">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Isi</label>
                                        <textarea class="form-control" id="isi" name="isi" placeholder="Isi..." rows="3">{{$LsuratKeluar[0]->suratkeluarisi ?? ''}}</textarea>
                                        <label for="isi" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>                                                
                                </div>
                                <div class="row">                                                                                                                                         
                                    <div class="col-md-12">
                                        <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Upload Surat</label>                                                    
                                            <div class="input-group">
                                                <button class="btn btn-primary btn-sm" type="submit" id="inputGroupFileAddon03">.pdf (Maks Size 2 Mb)</button>
                                                <input type="file" name="file" class="form-control form-control-sm" value="" id="file">                                                        
                                            </div>		                                    												                                                    
                                        <label for="file" generated="true" class="error"></label>
                                        <label id="validationError"></label>
                                    </div>
                                </div>
                                <br>
                                <div class="row">                                                                                                                                         
                                    <div class="col-md-12" align="left">
                                        <button type="submit" id="btn_simpan" class="btn btn-success btn-sm"><i class='bx bx-save mr-1'></i>Simpan</button>                                    
                                        <!-- <button type="button" id="btn_kembali" class="btn btn-danger btn-sm"><i class='bx bx-trash mr-1'></i>Kembali</button> -->
                                    </div>
                                </div>                                        
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('additional/js/add_surat_keluar.js') }}"></script>
