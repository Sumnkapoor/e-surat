var data_table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {
    
    loadData();
    $('[data-toggle="tooltip"]').tooltip();   
    $('button#tambah').on('click', function () {        
         clearModal();         
         $('#modal_label').text('Form Tambah Data');
         $('#method_field').val("POST");
         $(".modal-form").modal('show');
    });

    $('button#btn_simpan').on('click', function () {        
        insertUpdateProses();
    });    

    $('button#btn_simpan2').on('click', function () {        
        insertUpdateDetail();
    });    
        
});

    function loadData() {

        data_table  = $('#example2').DataTable({            
            processing: false,
            lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],     
            initComplete: function() {
                data_table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
                $("#example2").show();
            },
            buttons: ['excel', 'pdf', 'colvis'],       
            ajax: {
                "url": "" + base_url + '/getDataJson/jenisSurat',
                'type': 'GET',
                'dataType': 'JSON',
                'error': function (xhr, textStatus, ThrownException) {                    
                    error_noti('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
                }
            },

            columns: [
            {
                title: "Aksi",
                data: "jenissuratid",
                width: "10%",
                visible: true,
                sortable: false,
                class: "text-center",
                render: function (data, type, full, meta) {
                    var result = '';
                    result += '<td class="text-center">';
                    result += '<button title="Edit Data" class="btn btn-outline-warning btn-sm btn-edit px-2 ms-2"> <i class="bx bx-edit me-0"></i> </button>&nbsp;';
                    //result += '<button title="Detail" class="btn btn-outline-primary btn-sm btn-detail2 px-2 ms-2"> <i class="bx bx-search me-0"></i> </button>';
                    result += '<button title="Hapus Data" class="btn btn-outline-danger btn-sm btn-delete px-2 ms-2"> <i class="bx bx-trash me-0"></i> </button>';
                    result += '</td>';
                    return result;
                }
            },{
                title: "Nama Surat",                
                width: "",
                visible: true,
                sortable: true,
                class: "",
                render: function (data, type, row) {
                    return titleCase(row.jenissuratnama);
                }
            },{
                title: "Kode Surat",                
                width: "",
                visible: true,
                sortable: true,
                class: "",
                render: function (data, type, row) {
                    return titleCase(row.jenissuratkode);
                }
            }],

            "drawCallback": function (settings) {
                $('.btn-edit').on('click', function () {
                    clearModal();
                    var data = data_table.row($(this).parents('tr')).data();
                    $('#id').val(data.jenissuratid);
                    $('#kode_surat').val(data.jenissuratkode);
					$('#nama_surat').val(data.jenissuratnama);					                    

                    $('#modal_label').text('Form Ubah');
                    $('#method_field').val("PUT");
                    $(".modal-form").modal('show');
                });

                $('.btn-detail2').on('click', function () {
                    clearModal();
                    var data = data_table.row($(this).parents('tr')).data();                    
                    $('#id_jenis_surat').val(data.jenissuratid);
                    (data.jenissuratdetailperihal === "y") ? $("#perihal").prop("checked", true) : $("#perihal").prop("checked", false);
                    (data.jenissuratdetailkepada === "y") ? $("#kepada").prop("checked", true) : $("#kepada").prop("checked", false);
                    (data.jenissuratdetailjudul === "y") ? $("#judul").prop("checked", true) : $("#judul").prop("checked", false);
                    (data.jenissuratdetailisi === "y") ? $("#isi").prop("checked", true) : $("#isi").prop("checked", false);                    
                    (data.idjenissurat > 0) ? $('#method_field_detail').val("PUT") : $('#method_field_detail').val("POST");
                    $('#modal_label').text('Form Ubah');                    
                    $(".modal-form-detail").modal('show');                                        
                });

                $('.btn-delete').on('click', function () {
                    var data = data_table.row($(this).parents('tr')).data();
                    Lobibox.confirm({
                        iconClass: true,
                        title: 'Delete Data',                        
                        msg: 'Yakin Hapus Data "' + data.jenissuratnama + '"?',
                        callback: function ($this, type, ev) {
                            if(type=='yes'){
                                deleteProses(data.jenissuratid);
                            }        
                        }
                    });                    
                });
            }
		});            
    }

    function deleteProses(id) {
        $.ajax({
            type: 'GET',
            url: "" + base_url + "/delete/jenisSurat/" + id,
            dataType: 'JSON',            
            beforeSend: function(){
                BeforeSend();
            },
            complete: function(){
                AfterSend();
            },
            success: function (data) {
	            if (data.status == 'delete_successful') {
	                success_noti('Data Berhasil Terhapus');
	                data_table.ajax.reload(null, false);
	            } else if (data.status == 'delete_failed') {
	                error_noti('Data Gagal Dihapus');
	            } else {
                    error_noti('Data Gagal Dihapus (Kesalahan Sistem)');
                }
            },

            error: function (xmlhttprequest, textstatus, message) {
                error_noti('Koneksi Ke Server Gagal, Mohon Refresh Halaman')
            }
        });
	}

    function insertUpdateProses() {

        var form = $('#form');
        if (form.valid() == true) {
            
            var method = $('#method_field').val();
            var action_url = "" + base_url + "/jenisSurat";            
            var action_type = "Tambah";
            if (method === "PUT") {
                action_url = "" + base_url + "/jenisSurat/" + $('#id').val();
                action_type = "Ubah";
            }

            $.ajax({
                type: 'POST',
                url: action_url,
                dataType: 'JSON',
                data: form.serialize(),                
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {
                        success_noti('Berhasil ' + action_type + ' Data');
                        $('.modal-form').modal('toggle');
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal ' + action_type + ' Data'); 

                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');


                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }
    }

    function insertUpdateDetail() {

        var form2 = $('#form2');
        if (form2.valid() == true) {
                    
            var method = $('#method_field_detail').val();            
            var action_url = "" + base_url + "/detailSurat";                                    
            var action_type = "Tambah";
            if (method === "PUT") {
                action_url = "" + base_url + "/detailSurat/" + $('#id_jenis_surat').val();
                action_type = "Ubah";
            }

            $.ajax({
                type: method,
                url: action_url,
                dataType: 'JSON',
                data: form2.serialize(),                
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {
                        success_noti('Berhasil Tambah Data');
                        $('.modal-form-detail').modal('toggle');
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal Ubah Data'); 
                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');

                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }
    }

    var validator = $('#form').validate({

        rules: {
            kode_surat: {required: true},
            nama_surat: {required: true},			
        },

        highlight: function (element, errorClass, validClass, error) {
            $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
        },

        unhighlight: function (element, errorClass, validClass) {
            $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
        }
    });
