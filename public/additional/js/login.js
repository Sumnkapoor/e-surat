var data_table;

$(document).ready(function () {    

    $('div#btn_login').on('click', function () {

        loginProses();

    });

	$("#kelas").change(function(){

	  searchUsername(document.getElementById("kelas").value);
	});


});


function loginProses() {
	
	//alert($('#tanggal').val());
	 var form = $('#form_login_proses');
        if (form.valid() == true) {
            
            var method = $('#method_field').val();
            var action_url = "" + base_url + "/getDataLogin";                        

            $.ajax({
                type: 'POST',
                url: action_url,
                dataType: 'JSON',
                data: form.serialize(),
                beforeSend: function () {
                    sweetAlertLoading('Memproses Loading');
                },

                success: function (data) {
                    
                    if (data.status == 'insert_successful') {
                        sweetAlertDefault('<b>Berhasil Login</b>', 'success', 2000 );
                        $('.modal-form').modal('toggle');
                        window.location.href = './';
                        
                    } else if (data.status == 'insert_failed_password') {
                        sweetAlertDefault('<b>Pastikan User dan Password Anda Sudah Benar</b>', 'error', 2000 );                
                
                    } else {
                        sweetAlertDefault('<b>Pastikan User Anda Sudah Benar</b>', 'error', 2000 );                
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    sweetAlertDefault('<b>Koneksi Ke Server Gagal, '+message+'</b>', 'error', 2000 );
                }

            });            
            
        } else {                        
            sweetAlertLoading('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah',1000);
        }





}


function searchUsername(id){
	var method = $('#method_field').val();
	var action_url = "" + base_url + "/getDataJson/UserKelas";
	var action_type = "Tambah";

	$.ajax({

		type: 'POST',
		url: action_url,
		dataType: 'JSON',
		 headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: { id_kelas: id },



		success: function (data) {

			if (data.status == 'successful') {


			/*
				window.setInterval(function () {

					window.location.href = "" + base_url + "/karyawan";

				}, 1500);*/
				$("select#user_kelas").html(data.data_users);

			} else if (data.status == 'failed') {

				sweetAlertDefault('<b>Gagal ' + action_type + ' </b>', 'error', 2000 );



				var errors = data.error;

				errorValidationLaravel(errors, '#error-validation');

				$('html, body').animate({

					scrollTop: ($('.error').offset().top - 300)

				}, 1000);

			} else {

				sweetAlertDefault('<b>Gagal ' + action_type + ' (Kesalahan Sistem) </b>', 'error', 2000 );

			}

		},

		error: function (xmlhttprequest, textstatus, message) {

			sweetAlertDefault('<b>Koneksi Ke Server Gagalsss, Mohon Refresh Halaman</b>', 'error', 2000 );

		}

	});
}


var validator = $('#form_login_proses').validate({

    rules: {

		kelas: {

            required: true,

        },

        user_kelas: {

            required: true,

        },
		
		password: {

            required: true

        },

        
    },

    highlight: function (element, errorClass, validClass, error) {

        $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');

        $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');

        // $().find("[id=" + element.id + "]").removeClass('is-valid');

    },

    unhighlight: function (element, errorClass, validClass) {

        $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');

        $(element.form).find("[id=" + element.id + "]").addClass('is-valid');

    }

});



$('#btn_generate_pass').on("click", function(){

    var pass_generate = passGenerate(6);

    $('#password').val(pass_generate);

    $('#generate_result').show();

    $('#generate_result').text("Hasil Generate Password : "+pass_generate);

});

$('#password').on("keyup", function(){

    $('#generate_result').hide();

});



$("input[name='foto_identitas']").on("change", function () {

    showPreviewImage(1048576, this, 'foto_preview');

});

