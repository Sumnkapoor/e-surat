var data_table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {
    
    CKEDITOR.replace( 'perihal' );
    CKEDITOR.replace( 'isi' );    
    //CKEDITOR.replace( 'kepada' );    

    var idSuratKeluar = $('#id_surat_keluar').val();    

    if(idSuratKeluar > 0){
        $("#btn_simpan").prop("disabled", false);            
        $("#btn_generate").prop("disabled", true);          
    } else {        
        $("#btn_simpan").prop("disabled", true);    
        $("#btn_generate").prop("disabled", false);                    
    }    

    $("#no_surat").prop("readonly", true);    
   
    $('button#btn_generate').on('click', function () {        
        insertUpdateHeaderSurat();
    });
    
    $('button#btn_kembali').on('click', function () {        
        back();
    });
        
    $('.single-select').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.single-select2').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true
        }),
    
    $('.timepicker').pickatime()
        
    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });
        
});

    $('INPUT[type="file"]').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];

        if(this.files[0].size > 2000000) {            

            error_noti('Please upload file less than 2MB. Thanks!!');            
            $(this).val('');

        } else {

            switch (ext) {                
                case 'pdf':                            
                    break;
                default:
                    error_noti('File Yang Diperbolehkan Hanya Extension pdf');            
                    this.value = '';
            }
            
        }    
    });

    function back() {
        window.history.go(-1);return false;
    }    

    function insertUpdateHeaderSurat() {        
        var form = $('#formHeader');        
        
        if (form.valid() == true) {

            var jenisSurat = $("#jenis_surat option:selected").val();
            var kodeSurat = $("#jenis_surat").find(':selected').attr('data-kode');            
            
            //var method = $('#method_field_header').val();
            var action_url = "" + base_url + "/saveHeaderSuratKeluar";            
            var action_type = "Tambah";
            // if (method === "PUT") {
            //     action_url = "" + base_url + "/saveHeaderSuratKeluar/" + $('#idTr').val();
            //     action_type = "Ubah";
            // }
            
            $.ajax({
                type: 'POST',
                url: action_url,
                dataType: 'JSON',
                data: {
                    _token: CSRF_TOKEN,
                    jenisSurat: jenisSurat,
                    kodeSurat: kodeSurat
                },
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {                                                
                        success_noti('Berhasil ' + action_type + ' Data');
                        $("#tgl_surat").val(data.tgl);
                        $("#no_surat").val(data.nomerSurat);
                        $("#id_surat_keluar").val(data.idSuratKeluar);
                        $("#no_surat").prop("readonly", true);

                        $("#btn_generate").prop("disabled", true);
                        $("#btn_simpan").removeAttr('disabled');
                                            
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal ' + action_type + ' Data'); 

                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');


                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }
    }

    $('#formContent').submit(function(e) {
        e.preventDefault();

        var suratKeluarId = $('#id_surat_keluar').val();

        if(suratKeluarId > 0){

            var formData = new FormData(this);
            formData.append('perihal', CKEDITOR.instances['perihal'].getData());
            formData.append('isi', CKEDITOR.instances['isi'].getData());
            //formData.append('kepada', CKEDITOR.instances['kepada'].getData());
            var form = $('#formContent');        

            if (form.valid() == true) {
                
                //var method = $('#method_field_content').val();
                var action_url = "" + base_url + "/saveContentSuratKeluar";            
                var action_type = "Tambah";
                // if (method === "PUT") {
                //     action_url = "" + base_url + "/saveContentSuratKeluar/" + $('#id_surat_keluar').val();
                //     action_type = "Ubah";
                // }

                $.ajax({
                    type: 'POST',
                    url: action_url,
                    dataType: 'JSON',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        BeforeSend();
                    },
                    complete: function(){
                        AfterSend();
                    },
                    success: function (data) {
                        if (data.status == 'insert_successful') {
                            success_noti('Berhasil ' + action_type + ' Data');
                            $('.modal-form').modal('toggle');
                            data_table.ajax.reload(null, false);
                        } else if (data.status == 'insert_failed') {
                            error_noti('Gagal ' + action_type + ' Data'); 

                            var errors = data.error;
                            errorValidationLaravel(errors, '#error-validation');


                        } else {
                            error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                        }
                    },

                    error: function (xmlhttprequest, textstatus, message) {
                        error_noti('Koneksi Ke Server Gagal, '+message);
                    }

                });            
            
            } else {                        
                error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
            }        
        } else {
            error_noti('Silahkan Generate No. Surat Terlebih Dahulu');
        }
    });

    var validator = $('#formHeader').validate({

        rules: {
            jenis_surat: {required: true},            
        },

        highlight: function (element, errorClass, validClass, error) {
            $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
        },

        unhighlight: function (element, errorClass, validClass) {
            $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
        }
    });

    var validator = $('#formContent').validate({

        rules: {
            no_surat: {required: true},
            id_surat_keluar: {required: true},
            isi: {required: true},
        },

        highlight: function (element, errorClass, validClass, error) {
            $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
        },

        unhighlight: function (element, errorClass, validClass) {
            $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
        }
    });
