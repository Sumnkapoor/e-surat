var data_table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {

    CKEDITOR.replace( 'perihal' );
    $('button#btn_kembali').on('click', function () {        
        back();
    });    
        
    $('.single-select').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.single-select2').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true
        }),
    
    $('.timepicker').pickatime()
        
    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });
        
});


    $('INPUT[type="file"]').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];

        if(this.files[0].size > 2000000) {            

            error_noti('Please upload file less than 2MB. Thanks!!');            
            $(this).val('');

        } else {

            switch (ext) {                
                case 'pdf':                            
                    break;
                default:
                    error_noti('File Yang Diperbolehkan Hanya Extension pdf');            
                    this.value = '';
            }
            
        }    
    });

    function back() {
        window.history.go(-1);return false;
    }
    
    $('#form_upload').submit(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        formData.append('perihal', CKEDITOR.instances['perihal'].getData());
        var form = $('#form_upload');                

        if (form.valid() == true) {
            
            var method = $('#method_field').val();
            var action_url = "" + base_url + "/saveSuratMasuk";            
            var action_type = "Tambah";
            if (method === "PUT") {
                action_url = "" + base_url + "/saveSuratMasuk/" + $('#idTr').val();
                action_type = "Ubah";
            }

            $.ajax({
                type: 'POST',
                url: action_url,
                dataType: 'JSON',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {
                        success_noti('Berhasil ' + action_type + ' Data');
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal ' + action_type + ' Data'); 

                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');

                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }        
    });

    // var validator = $('#form').validate({

    //     rules: {
    //         judul: {required: true},
    //         kepada: {required: true},			            
    //     },

    //     highlight: function (element, errorClass, validClass, error) {
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
    //     },

    //     unhighlight: function (element, errorClass, validClass) {
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
    //     }
    // });
