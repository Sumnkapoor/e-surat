var data_table;

$(document).ready(function () {
    
    loadData();

    $('.single-select').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.single-select2').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true
        }),
    
    $('.timepicker').pickatime()
        
    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });

    $('button#tambah').on('click', function () {        
         clearModal();         
         $('#modal_label').text('Form Tambah Data');
         $('#method_field').val("POST");
         $(".modal-form").modal('show');
    });

    $('button#btn_simpan').on('click', function () {        
        insertUpdateProses();
    });    
        
});

    $('.actionx').click(function(event){			      			                  
        var urlx = $(this).attr('data-href');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');		

        $.ajax({
            url: "" + base_url + urlx,
            type: 'get',
            cache: false,
            data: {
                _token: CSRF_TOKEN,                
            },
            beforeSend: function(){
                BeforeSend();
            },
            complete: function(){
                AfterSend();
            },									
            success: function (data) {					
                $('.isiContent').html(data.html);					
            },

            error: function (xhr, status, error, xmlhttprequest, textstatus, message) {						
                alert(xmlhttprequest+"/"+textstatus+"/"+message);																						
            }
        });				
    });

    function loadData() {        
        data_table  = $('#example2').DataTable({            
            processing: false,
            lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],            
            initComplete: function() {
                data_table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
                $("#example2").show();
            },
            buttons: ['excel', 'pdf', 'colvis'],
            ajax: {
                "url": "" + base_url + '/getDataJson/departement',
                'type': 'GET',
                'dataType': 'JSON',
                'error': function (xhr, textStatus, ThrownException) {                    
                    error_noti('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
                }
            },

            columns: [
            {
                title: "Aksi",
                data: "kodeunit",
                width: "15%",
                visible: true,
                sortable: false,
                class: "text-center",
                render: function (data, type, full, meta) {
                    var result = '';
                    result += '<td class="text-center">';
                    result +=
                        '<button title="Edit Data" class="btn btn-outline-warning btn-sm btn-edit px-2 ms-2"> <i class="bx bx-edit me-0"></i> </button>&nbsp;';                        
                    // result +=
                    //     '<button class="btn btn-danger  btn-sm btn-delete"> <i class="bx bx-trash"></i> </button>';
                    result += '</td>';
                    return result;
                }
            },{
                title: "Nama Unit ",
                data: "namaunit",
                width: "20%",
                visible: true,
                sortable: true,
                class: ""
            },{
                title: "Level",
                data: "level",
                width: "5%",
                visible: true,
                sortable: true,
                class: ""
            },{
                title: "Kode Unit",
                data: "kodeunit",
                width: "10%",
                visible: true,
                sortable: true,
                class: ""
            },{
                title: "Kode Unit Parent",
                data: "kodeunitparent",
                width: "10%",
                visible: true,
                sortable: true,
                class: ""
            },{
                title: "Kode Surat",
                data: "kodesurat",
                width: "10%",
                visible: true,
                sortable: true,
                class: ""
            }],

            "drawCallback": function (settings) {
                $('.btn-edit').on('click', function () {
                    clearModal();
                    var data = data_table.row($(this).parents('tr')).data();
                    $('#kodeunit').val(data.kodeunit);                    					

                    $('#modal_label').text('Form Ubah');
                    $('#method_field').val("PUT");
                    $(".modal-form").modal('show');
                });

                // $('.btn-delete').on('click', function () {
                //     var data = data_table.row($(this).parents('tr')).data();
                //     Lobibox.confirm({
                //         iconClass: true,
                //         title: 'Delete Data',                        
                //         msg: 'Yakin Hapus Data "' + data.nama_perkiraan + '"?',
                //         callback: function ($this, type, ev) {
                //             if(type=='yes'){
                //                 deleteProses(data.id);
                //             }        
                //         }
                //     });                    
                // });
            }
		});            
    }

    function deleteProses(id) {
        // $.ajax({
        //     type: 'GET',
        //     url: "" + base_url + "/delete/edit_perkiraan/" + id,
        //     dataType: 'JSON',            

        //     success: function (data) {
	    //         if (data.status == 'delete_successful') {
	    //             success_noti('Data Berhasil Terhapus');
	    //             data_table.ajax.reload(null, false);
	    //         } else if (data.status == 'delete_failed') {
	    //             error_noti('Data Gagal Dihapus');
	    //         } else {
        //             error_noti('Data Gagal Dihapus (Kesalahan Sistem)');
        //         }
        //     },

        //     error: function (xmlhttprequest, textstatus, message) {
        //         error_noti('Koneksi Ke Server Gagal, Mohon Refresh Halaman')
        //     }
        // });
	}

    function insertUpdateProses() {

        var form = $('#form2');
        if (form.valid() == true) {
            
            var method = $('#method_field').val();
            var action_url = "" + base_url + "/departement";            
            var action_type = "Tambah";
            if (method === "PUT") {
                action_url = "" + base_url + "/departement/" + $('#kodeunit').val();
                action_type = "Ubah";
            }

            $.ajax({
                type: 'POST',
                url: action_url,
                dataType: 'JSON',
                data: form.serialize(),                
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {
                        success_noti('Berhasil ' + action_type + ' Data');
                        $('.modal-form').modal('toggle');
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal ' + action_type + ' Data'); 

                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');


                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }
    }



    // var validator = $('#form').validate({

    //     rules: {
    //         kode_perkiraan: {required: true},
    //         nama_perkiraan: {required: true},			
    //     },

    //     highlight: function (element, errorClass, validClass, error) {
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
    //     },

    //     unhighlight: function (element, errorClass, validClass) {
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
    //     }
    // });
